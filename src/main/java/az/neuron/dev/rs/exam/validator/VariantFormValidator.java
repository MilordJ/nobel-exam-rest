/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.validator;

import az.neuron.dev.rs.exam.enums.Constants;
import az.neuron.dev.rs.exam.form.VariantForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author RUHANI ALIYEV
 */
public class VariantFormValidator implements Validator {

    private final String type;

    public VariantFormValidator(String type) {
        this.type = type;
    }

    @Override
    public boolean supports(Class<?> type) {
        return VariantForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        VariantForm form = (VariantForm) o;

        if (Constants.VALIDATOR_ADD.equals(type) || Constants.VALIDATOR_EDIT.equals(type)) {
             if (form.getContent()== null || form.getContent().trim().isEmpty()) {
                errors.reject("empty variant content");
            }

        }
    }

}
