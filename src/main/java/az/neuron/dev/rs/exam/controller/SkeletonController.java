/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.controller;

import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.enums.ResultCode;
import az.neuron.dev.rs.exam.exception.HsisException;
import az.neuron.dev.rs.exam.exception.AuthenticationException;
import az.neuron.dev.rs.exam.exception.InvalidParametersException;
import az.neuron.dev.rs.exam.service.FtpService;
import az.neuron.dev.rs.exam.service.ExamService;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author Nazrin
 */

@Controller
public class SkeletonController extends AbstractController {
    
    @Value("${ftp.root.directory}")
    protected String rootDirectory;
    
    @Autowired
    protected HttpServletRequest request;
    
    @Autowired
    protected ExamService service;
    
    @Autowired
    protected FtpService ftpService;
    
    public SkeletonController() {
    }
    
    protected ModelAndView handleRequestInternal(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
     protected User checkToken(OperationResponse operationResponse, String token, String... params) throws HsisException {
       if(token == null || token.trim().isEmpty()) {
            operationResponse.setCode(ResultCode.UNAUTHORIZED);
            throw new InvalidParametersException("Token is null");
        } 
       
       if(params != null) {
            for(String s: params) {
                if(s == null || s.trim().isEmpty()) {
                    operationResponse.setCode(ResultCode.INVALID_PARAMS);
                    throw new InvalidParametersException("Required Parameter is null");
                }
            }
        }
       
       User user = service.checkToken(token);
       
       if(user == null) {
           operationResponse.setCode(ResultCode.UNAUTHORIZED);
           throw new AuthenticationException("Invalid token!");
       }
       
       return user;
    } 
    
    protected String getSecurityToken() {
        return request.getHeader("Token");
    }
     
    protected User checkToken(String token) throws HsisException {
        if(token == null || token.trim().isEmpty()) {
            throw new InvalidParametersException("Token is null");
        } 
        
        User user = service.checkToken(token);
       
       if(user == null) {
           throw new AuthenticationException("Invalid token!");
       }
       
       return user;
    }
    
}
