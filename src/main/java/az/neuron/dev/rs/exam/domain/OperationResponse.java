/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import az.neuron.dev.rs.exam.enums.ResultCode;
import lombok.Data;

/**
 *
 * @author Nazrin
 */

@Data
public class OperationResponse {
    private ResultCode code;
    private MultilanguageString message;
    private Object data;

    public OperationResponse(ResultCode code) {
        this.code = code;
    }

    public OperationResponse(ResultCode code, Object data) {
        this.code = code;
        this.data = data;
    }

    public OperationResponse(ResultCode code, MultilanguageString message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

}
