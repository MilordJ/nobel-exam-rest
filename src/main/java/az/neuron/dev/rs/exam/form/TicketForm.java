/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.form;

import lombok.Data;

/**
 *
 * @author Lito
 */
@Data
public class TicketForm extends BaseForm{
    private Integer id;
    private Integer ticketGroup;
    private Integer ticketFaculty;
    private Integer ticketDepartment;
    private Integer[] subject;
    private Integer subjectId;
    private Integer eduPlanId;
    private Integer examType;
    private Integer ticketCount;
    private String questNote;
    private Integer keyword;
    private Integer[] questTopic;
    private QuestStructureForm[] structure;
    private Integer page = 1;
    private Integer pageSize = 30;
}
