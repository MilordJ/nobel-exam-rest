/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import lombok.Data;

/**
 *
 * @author Nazrin
 */

@Data
public class FileWrapper {
    private int id;
    private String originalName;
    private String path;
    private byte[] file;

    public FileWrapper() {
    }

    public FileWrapper(int id) {
        this.id = id;
    }

    public FileWrapper(int id, String originalName, String path) {
        this.id = id;
        this.originalName = originalName;
        this.path = path;
    }

    public FileWrapper(String path) {
        this.path = path;
    }
    

    public FileWrapper(int id, String originalName, String path, byte[] file) {
        this.id = id;
        this.originalName = originalName;
        this.path = path;
        this.file = file;
    }

  
    
}
