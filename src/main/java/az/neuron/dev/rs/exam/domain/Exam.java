/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import java.util.List;
import lombok.Data;

/**
 *
 * @author RUHANI ALIYEV
 */
@Data
public class Exam {
    
    private int id;
    private DictionaryWrapper course;
    private DictionaryWrapper subject;
    private DictionaryWrapper org;
    private String examStartDate;
    private String examFinishDate;
    private String examStartTime;
    private String examFinishTime;
    private String examDuration;
    private int participantId;
    private List<Integer> participants;
    private int pelcId;
    private int ticketId;
    private int pointBarrier;
    private ExamFinishedResult efr;
    private List<ExamResult> er;
    
    public Exam(int id, DictionaryWrapper course, String examStartDate, String examFinishDate, String examStartTime, String examFinishTime) {
    this.id = id;
    this.course = course;
    this.examStartDate = examStartDate;
    this.examFinishDate = examFinishDate;
    this.examStartTime = examStartTime;
    this.examFinishTime = examFinishTime;
    }

    public Exam(int id, DictionaryWrapper org, String examStartDate, String examFinishDate, String examStartTime, String examFinishTime, String examDuration) {
        this.id = id;
        this.org = org;
        this.examStartDate = examStartDate;
        this.examFinishDate = examFinishDate;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
        this.examDuration = examDuration;
    }

    public Exam(int id, DictionaryWrapper org, String examStartDate, String examFinishDate, String examStartTime, String examFinishTime, String examDuration, int pointBarrier, int participantId) {
        this.id = id;
        this.org = org;
        this.examStartDate = examStartDate;
        this.examFinishDate = examFinishDate;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
        this.examDuration = examDuration;
        this.pointBarrier = pointBarrier;
        this.participantId = participantId;
    }

    public Exam(int id, List<Integer> participants, DictionaryWrapper org, String examStartDate, String examFinishDate, String examStartTime, String examFinishTime, String examDuration, int pointBarrier) {
        this.id = id;
        this.participants = participants;
        this.org = org;
        this.examStartDate = examStartDate;
        this.examFinishDate = examFinishDate;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
        this.examDuration = examDuration;
        this.pointBarrier = pointBarrier;
    }

    public Exam(int id, DictionaryWrapper subject, String examStartDate, String examFinishDate, String examStartTime, String examFinishTime, String examDuration, int participantId) {
        this.id = id;
        this.subject = subject;
        this.examStartDate = examStartDate;
        this.examFinishDate = examFinishDate;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
        this.examDuration = examDuration;
        this.participantId = participantId;
    }

    public Exam(int id, DictionaryWrapper course, DictionaryWrapper subject, String examStartDate, String examFinishDate, String examStartTime, String examFinishTime, String examDuration, int participantId, int pelcId, int ticketId) {
        this.id = id;
        this.course = course;
        this.subject = subject;
        this.examStartDate = examStartDate;
        this.examFinishDate = examFinishDate;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
        this.examDuration = examDuration;
        this.participantId = participantId;
        this.pelcId = pelcId;
        this.ticketId = ticketId;
    }

    public Exam(ExamFinishedResult efr, List<ExamResult> er) {
        this.efr = efr;
        this.er = er;
    }
    
    public Exam() {
    }

    
}
