/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.util;


import az.neuron.dev.rs.exam.domain.DictionaryWrapper;
import az.neuron.dev.rs.exam.domain.FileWrapper;
import az.neuron.dev.rs.exam.domain.MultilanguageString;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.domain.UserAccount;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Nazrin
 */
public class RowFetcher {

@Autowired
    
    public static final User fetchUser(ResultSet resultSet) throws SQLException {
        return new User(0, 
                        new UserAccount(resultSet.getInt("user_id"), 
                                        resultSet.getString("user_name"), 
                                        "",
                                        new DictionaryWrapper(resultSet.getInt("user_role_id")),
                                        resultSet.getInt("is_blocked") == 1), 
                        resultSet.getString("person_name"), 
                        resultSet.getString("person_surname"), 
                        resultSet.getString("person_patronymic"), 
                        resultSet.getString("person_birthdate"), 
                        new DictionaryWrapper(resultSet.getInt("gender_id")), 
                        resultSet.getString("person_pin"), 
                        new FileWrapper(resultSet.getInt("person_file_id"), null, null),
                        resultSet.getInt("active_sessions") > 0);
    }
    
   
}
