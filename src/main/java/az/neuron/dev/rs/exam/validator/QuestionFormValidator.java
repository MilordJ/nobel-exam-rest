/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.validator;

import az.neuron.dev.rs.exam.enums.Constants;
import az.neuron.dev.rs.exam.form.QuestionSearchForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author RUHANI ALIYEV
 */
public class QuestionFormValidator implements Validator{
    private final String type;

    public QuestionFormValidator(String type) {
        this.type = type;
    }
    
    

    @Override
    public boolean supports(Class<?> type) {
        return QuestionSearchForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
          QuestionSearchForm form = (QuestionSearchForm) o;
        
     
        if(Constants.VALIDATOR_ADD.equals(type)) {

//            if (form.getEduLevelId() == null || form.getEduLevelId() <= 0) {
//                errors.reject("Empty edu level id");
//            }
            
            if(form.getQuestionLevelId() == null || form.getQuestionLevelId() <= 0) {
                errors.reject("Empty question level id");
            }
            
//            if(form.getQuestionTypeId() == null || form.getQuestionTypeId() <= 0) {
//                errors.reject("Empty question type id");
//            }
            if(form.getLangId() == null || form.getLangId() <= 0) {
                errors.reject("Empty question lang id");
            }
//            if (form.getContent()== null || form.getContent().trim().isEmpty()) {
//                errors.reject("empty question content");
//            }
            
            if(form.getSubjectId() == null || form.getSubjectId() <= 0) {
                errors.reject("empty question subject");
            }
        }else if(Constants.VALIDATOR_EDIT.equals(type)) {

            if (form.getId() <= 0) {
                errors.reject("Empty question id");
            }
//            
//            if (form.getEduLevelId() == null || form.getEduLevelId() <= 0) {
//                errors.reject("Empty edu level id");
//            }
            
            if(form.getQuestionLevelId() == null || form.getQuestionLevelId() <= 0) {
                errors.reject("Empty question level id");
            }
            
//            if(form.getQuestionTypeId() == null || form.getQuestionTypeId() <= 0) {
//                errors.reject("Empty question type id");
//            }
            if(form.getLangId() == null || form.getLangId() <= 0) {
                errors.reject("Empty question lang id");
            }
//            if (form.getContent()== null || form.getContent().trim().isEmpty()) {
//                errors.reject("empty question content");
//            }
            
            if(form.getSubjectId() == null || form.getSubjectId() <= 0) {
                errors.reject("empty question subject");
            }
        }
       
        
    }
    
}
