/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import lombok.Data;

/**
 * @author Lito
 */
@Data
public class QuestionChoises {
    private int id;
    private int questId;
    private String questionContent;
    private int rightChoise;
    private FileWrapper fileWrapper;

    public QuestionChoises(int id, int questId, String questionContent, int rightChoise, FileWrapper fileWrapper) {
        this.id = id;
        this.questId = questId;
        this.questionContent = questionContent;
        this.rightChoise = rightChoise;
        this.fileWrapper = fileWrapper;
    }
    public QuestionChoises(int id, int questId, String questionContent, FileWrapper fileWrapper) {
        this.id = id;
        this.questId = questId;
        this.questionContent = questionContent;
        this.fileWrapper = fileWrapper;
    }

}
