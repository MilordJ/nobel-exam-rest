/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.form;

import lombok.Data;

/**
 *
 * @author RUHANI ALIYEV
 */

@Data
public class QuestionSearchForm  extends BaseForm{
    private int id;
    private Integer subjectId;
    private Integer eduPlanId;
    private Integer eduLevelId;
    private Integer questionLevelId;
    private Integer questionTypeId;
    private String keyword;
    private String orderColumn = "id";
    private String orderType = "asc";
    private Integer page = 1;
    private Integer pageSize = 20;
    private FileWrapperForm fileForm;
    private VariantForm[] variants;
    private Integer topicId;
    private Integer langId;
    private Integer tipiId;
    private String content;

  
}
