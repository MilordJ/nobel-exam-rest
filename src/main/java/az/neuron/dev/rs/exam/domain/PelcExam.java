/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import java.util.List;
import lombok.Data;

/**
 *
 * @author RUHANI ALIYEV
 */
@Data
public class PelcExam {

    private int id;
    private int ticketId;
    private int participantId;
    private String startDate;
    private String endDate;
    private String startTime;
    private String endTime;
    private String duration;
    private DictionaryWrapper courseId;
//  private List<Questions> questions;
    private List<ExamResult> questions;

    public PelcExam(int id, int ticketId, int participantId, String startDate, String endDate, String startTime, String endTime, String duration, List<ExamResult> questions) {
        this.id = id;
        this.ticketId = ticketId;
        this.participantId = participantId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.duration = duration;
        this.questions = questions;
    }
    
}
