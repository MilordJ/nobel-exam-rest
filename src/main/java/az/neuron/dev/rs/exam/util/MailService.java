/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.util;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

/**
 *
 * @author Lito
 */
public class MailService {

    private static final class SenderInfo extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            String username = "nobel.oil.system@gmail.com";
            String password = "neuron2017";
            return new PasswordAuthentication(username, password);
        }
    }

    private static final Logger log = Logger.getLogger(MailService.class);

    public static void Send(String text, String subject, String recieverAddress) {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.starttls.enable", "true");
//        SenderInfo sender = new SenderInfo();
        Session mailSession = Session.getDefaultInstance(props, new MailService.SenderInfo());
        try {
//            if(!recieverAddress.equals("unal.jafarli@neuron.az")){
            MimeMessage message = new MimeMessage(mailSession);
            message.setFrom(new InternetAddress("nobel.oil.system@gmail.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recieverAddress));
            message.setSubject(subject);

            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setText("This is message body");
            String filename = "C:\\Users\\Lito\\Desktop\\U\\Unal\\New folder\\unnamed.jpg";
//            DataSource source = new FileDataSource(filename);
            Multipart multipart = new MimeMultipart(); //1
// Create the attachment part
            BodyPart attachmentBodyPart = new MimeBodyPart(); //2
//            attachmentBodyPart.setDataHandler(new DataHandler(source)); //2
//            attachmentBodyPart.setFileName("unnamed.jpg"); // 2
//            multipart.addBodyPart(attachmentBodyPart); //3
// Create the HTML Part
            String htmlMessageAsString = text;
            BodyPart htmlBodyPart = new MimeBodyPart(); //4
            htmlBodyPart.setContent(htmlMessageAsString, "text/html; charset=utf-8"); //5
            multipart.addBodyPart(htmlBodyPart); // 6
// Set the Multipart's to be the email's content
            message.setContent(multipart); //7

            // Send message
            Transport.send(message);

            log.info("Message was sent!");
//        }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
