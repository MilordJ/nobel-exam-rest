/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import java.util.List;
import lombok.Data;

/**
 *
 * @author J
 */
@Data
public class Questions {

    private int id;
    private String content;
    private String filePath;
    private DictionaryWrapper eduPlan;
    private DictionaryWrapper subject;
    private DictionaryWrapper level;
    private DictionaryWrapper questionType;
    private DictionaryWrapper eduLevel;  
    private DictionaryWrapper topic;
    private DictionaryWrapper language;
    private DictionaryWrapper tipi;
    private DictionaryWrapper eduPart;
    private DictionaryWrapper eduYear;  
    private DictionaryWrapper course;  
    private DictionaryWrapper department;  
    private DictionaryWrapper faculty;  
    private DictionaryWrapper org;  
    private List<QuestionChoises> choises;
    private FileWrapper fileWrapper;
    private List<Integer> userAnswers;
    List<ExamResult> answerlist;
//    private int rightAnswerCount;

    public Questions() {
    }

    public Questions(int id, String content, DictionaryWrapper subject, DictionaryWrapper level, DictionaryWrapper questionType, DictionaryWrapper topic, DictionaryWrapper language, FileWrapper fileWrapper, List<QuestionChoises> choises) {
        this.id = id;
        this.content = content;
        this.subject = subject;
        this.level = level;
        this.questionType = questionType;
        this.topic = topic;
        this.language = language;
        this.choises = choises;
        this.fileWrapper = fileWrapper;
    }

    public Questions(int id, String content, DictionaryWrapper subject, DictionaryWrapper level, DictionaryWrapper questionType, DictionaryWrapper topic) {
        this.id = id;
        this.content = content;
        this.subject = subject;
        this.level = level;
        this.questionType = questionType;
        this.topic = topic;
    }

    public Questions(int id, String content, String filePath, DictionaryWrapper subject, DictionaryWrapper eduPart, DictionaryWrapper eduYear, DictionaryWrapper course, DictionaryWrapper department, DictionaryWrapper faculty) {
        this.id = id;
        this.content = content;
        this.filePath = filePath;
        this.subject = subject;
        this.eduPart = eduPart;
        this.eduYear = eduYear;
        this.course = course;
        this.department = department;
        this.faculty = faculty;
    }

    public Questions(int id, String content, String filePath, List<QuestionChoises> choises) {
        this.id = id;
        this.content = content;
        this.filePath = filePath;
        this.choises = choises;
    }

    public Questions(int id, List<ExamResult> answerlist, String content, String filePath, List<QuestionChoises> choises) {
        this.id = id;
        this.answerlist = answerlist;
//        this.rightAnswerCount = rightAnswerCount;
        this.content = content;
        this.filePath = filePath;
        this.choises = choises;
    }
//
//    public Questions(int id, String content, String filePath, DictionaryWrapper subject, DictionaryWrapper org, List<QuestionChoises> choises) {
//        this.id = id;
//        this.content = content;
//        this.filePath = filePath;
//        this.subject = subject;
//        this.org = org;
//        this.choises = choises;
//    }
    
}
