/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.form;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author RUHANI ALIYEV
 */

@Data
public class FileWrapperForm {
    private String path;
    private MultipartFile file;
    private int id;

    public FileWrapperForm() {
    }

    public FileWrapperForm(String path, MultipartFile file) {
        this.path = path;
        this.file = file;
    }

    public FileWrapperForm(String path, MultipartFile file, int id) {
        this.path = path;
        this.file = file;
        this.id = id;
    }

    
}
