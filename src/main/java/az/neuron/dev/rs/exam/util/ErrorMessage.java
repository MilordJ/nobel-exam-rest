/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.util;

import az.neuron.dev.rs.exam.domain.MultilanguageString;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author RUHANI ALIYEV
 */
public class ErrorMessage {

    public static final Map<Integer, MultilanguageString> message;

    static {
        message = new HashMap<Integer, MultilanguageString>();
        message.put(20028, new MultilanguageString("Bu məlumatı halhazırda silmək mümkün deyil. Daxilində digər məlumat var.",
                "",
                ""));
        message.put(20150, new MultilanguageString("İmtahan artıq yaradılıb",
                "",
                ""));
        message.put(20006, new MultilanguageString("Sessiya bitib",
                "",
                ""));
        message.put(20016, new MultilanguageString("Tarix səhv qeyd edilib",
                "",
                ""));
        message.put(20513, new MultilanguageString("Tələbə akademik qrupa bərkidilib",
                "",
                ""));
        message.put(20008, new MultilanguageString("Daxil edilən parametrlərə uyğun sual yoxdur",
                "",
                ""));
        message.put(20009, new MultilanguageString("Məlumatın sayı düzgün deyil",
                "",
                ""));
        message.put(20018, new MultilanguageString("Faylların sayı düzgün deyil",
                "",
                ""));
        message.put(20403, new MultilanguageString("Hərəkət tapilmadı",
                "",
                ""));
        message.put(20020, new MultilanguageString("Fayl tapilmadı",
                "",
                ""));
        message.put(20089, new MultilanguageString("Əməliyyat üçün müvafiq imtiyaz tələb olunur",
                "",
                ""));
        message.put(20515, new MultilanguageString("Seçilmiş mövzuları əhatə edən sual yoxdur",
                "",
                ""));
        message.put(20888, new MultilanguageString("İmtahan istifadə edilib",
                "",
                ""));
        message.put(20889, new MultilanguageString("İmtahan vaxtı düzgün təyin edilməyib!!!",
                "",
                ""));
        message.put(20890, new MultilanguageString("İmtahan bərkidilmiş bilet!!!",
                "",
                ""));
        message.put(20891, new MultilanguageString("Biletə bərkidilmiş sual!!!",
                "",
                ""));

    }

}
