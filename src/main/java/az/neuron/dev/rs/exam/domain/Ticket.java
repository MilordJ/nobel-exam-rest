/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import java.util.List;
import lombok.Data;

/**
 *
 * @author RUHANI ALIYEV
 */
@Data
public class Ticket {

    private int id;
    private String ticketNote;
    private DictionaryWrapper subjectId;

    public Ticket(int id, String ticketNote) {
        this.id = id;
        this.ticketNote = ticketNote;
//        this.subjectId = subjectId;
    }

    
}
