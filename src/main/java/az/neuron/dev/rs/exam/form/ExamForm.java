/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.form;

import az.neuron.dev.rs.exam.domain.FileWrapper;
import lombok.Data;

/**
 *
 * @author RUHANI ALIYEV
 */

@Data
public class ExamForm  extends BaseForm{
    
    private int id;
    private int examTypeId;
    private Integer courseId;
    private String examStartDate;
    private String examFinishDate;
    private String examStartTime;
    private String examFinishTime;
    private int examDuration;
    private Integer subjectId;
    private Integer participants [];
    private String participantName;
    private String search;
    private String examDate;
    private String examCode;
    private Integer examResultCode;
    private Integer pelcId;
    private Integer orgId;
    private Integer repetition;
    private int resultVisibility;
    private int pointBarrier;
    private Integer page = 1;
    private Integer pageSize = 20;

}
