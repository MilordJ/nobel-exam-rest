/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import lombok.Data;

/**
 *
 * @author Lito
 */
@Data
public class QuestAndAnswer {
    int questId;
    int answerId;

    public QuestAndAnswer(int questId, int answerId) {
        this.questId = questId;
        this.answerId = answerId;
    }
    
    
}
