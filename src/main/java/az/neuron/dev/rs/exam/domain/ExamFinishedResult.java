/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import lombok.Data;

/**
 *
 * @author Lito
 */
@Data
public class ExamFinishedResult {
    
    User user;
    int id;
    int examId;
    DictionaryWrapper subject;
    int examParticipantId;
    int personId;
    int correctQuest;
    int ansquestPercent;
    int questCount;
    boolean passed;
    int rightCount;
    int falseCount;
    int emptyCount;
    int participantId;
    int pelcId;
    int ticketId;
    int resultPercent;
    int examResultPoint;
    int examDurationTime;
    String code;
    String examStartDate;
    String examDate;
    int pointBarrier;
    int userPercent;
    String examFinishDate;
    String examStartTime;
    String examFinishTime;
    String pelcStartTime;
    String pelcFinishTime;
    String mailAddress;
    DictionaryWrapper course;

    public ExamFinishedResult() {
    }
    
    public ExamFinishedResult(int questCount, int rightCount, int falseCount, int emptyCount, String mailAddress) {
        this.questCount = questCount;
        this.rightCount = rightCount;
        this.falseCount = falseCount;
        this.emptyCount = emptyCount;
        this.mailAddress = mailAddress;
    }    
    
    public ExamFinishedResult(int questCount, int rightCount, int falseCount, int emptyCount, String mailAddress, boolean passed, int resultPercent) {
        this.questCount = questCount;
        this.rightCount = rightCount;
        this.falseCount = falseCount;
        this.emptyCount = emptyCount;
        this.mailAddress = mailAddress;
        this.passed = passed;
        this.resultPercent = resultPercent;
    }    

    public ExamFinishedResult(User user, int examId, int examParticipantId, int personId, int questCount, int rightCount, int falseCount, int emptyCount, int participantId, int pelcId, int ticketId, int examResultPoint, String examStartDate, String examFinishDate, String examStartTime, String examFinishTime, String pelcStartTime, String pelcFinishTime) {
        this.user = user;
        this.examId = examId;
        this.examParticipantId = examParticipantId;
        this.personId = personId;
        this.questCount = questCount;
        this.rightCount = rightCount;
        this.falseCount = falseCount;
        this.emptyCount = emptyCount;
        this.participantId = participantId;
        this.pelcId = pelcId;
        this.ticketId = ticketId;
        this.examResultPoint = examResultPoint;
        this.examStartDate = examStartDate;
        this.examFinishDate = examFinishDate;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
        this.pelcStartTime = pelcStartTime;
        this.pelcFinishTime = pelcFinishTime;
    }

    public ExamFinishedResult(int examId, User user, int id, DictionaryWrapper subject, int examParticipantId, int ticketId, int examDurationTime, String examStartDate, String examStartTime, String examFinishTime, String pelcStartTime, String pelcFinishTime, int correctQuest, int ansquestPercent, int questCount) {
        this.examId = examId;
        this.user = user;
        this.id = id;
        this.subject = subject;
        this.examParticipantId = examParticipantId;
        this.ticketId = ticketId;
        this.examDurationTime = examDurationTime;
        this.examStartDate = examStartDate;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
        this.pelcStartTime = pelcStartTime;
        this.pelcFinishTime = pelcFinishTime;
        this.correctQuest = correctQuest;
        this.ansquestPercent = ansquestPercent;
        this.questCount = questCount;
    }

    public ExamFinishedResult(int examId, int examParticipantId, DictionaryWrapper subject, int correctQuest, int ansquestPercent, int questCount, int examDurationTime, String examStartDate, String examFinishDate, String examStartTime, String examFinishTime) {
        this.examId = examId;
        this.examParticipantId = examParticipantId;
        this.subject = subject;
        this.correctQuest = correctQuest;
        this.ansquestPercent = ansquestPercent;
        this.questCount = questCount;
        this.examDurationTime = examDurationTime;
//        this.code = code;
        this.examStartDate = examStartDate;
        this.examFinishDate = examFinishDate;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
    }

    public ExamFinishedResult(User user, int id, int examParticipantId, int rightCount, int falseCount, int emptyCount, String examDate, int pointBarrier, int userPercent, String examStartTime, String examFinishTime) {
        this.user = user;
        this.id = id;
        this.examParticipantId = examParticipantId;
        this.rightCount = rightCount;
        this.falseCount = falseCount;
        this.emptyCount = emptyCount;
        this.examDate = examDate;
        this.pointBarrier = pointBarrier;
        this.userPercent = userPercent;
        this.examStartTime = examStartTime;
        this.examFinishTime = examFinishTime;
    }
    
    
}
