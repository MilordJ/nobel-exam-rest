/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import lombok.Data;

/**
 *
 * @author RUHANI ALIYEV
 */

@Data
public class Subject {
    private int id;
    private DictionaryWrapper subject;
    private DictionaryWrapper org;
    private String startDate;
    private String endDate;

    public Subject() {
    }

    public Subject(int id, DictionaryWrapper subject, DictionaryWrapper org, String startDate, String endDate) {
        this.id = id;
        this.subject = subject;
        this.org = org;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Subject(int id, DictionaryWrapper subject) {
        this.id = id;
        this.subject = subject;
    }
    
    
    
}
