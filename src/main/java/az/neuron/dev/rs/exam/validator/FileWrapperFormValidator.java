/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.validator;

import az.neuron.dev.rs.exam.enums.Regex;
import az.neuron.dev.rs.exam.form.FileWrapperForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author RUHANI ALIYEV
 */
public class FileWrapperFormValidator implements Validator {
    private final String type;

    public FileWrapperFormValidator(String type) {
        this.type = type;
    }
    

    @Override
    public boolean supports(Class<?> type) {
        return FileWrapperForm.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        FileWrapperForm form = (FileWrapperForm) o;
        
     
        
        if( form.getFile()!= null && form.getFile().getSize()>0 && !form.getFile().getContentType().matches(type)) {
            errors.reject("invalid file content type");
        }
        
        if(form.getFile()!= null && form.getFile().getSize()>(5*1024*1024)) {
            errors.reject(" file size must be less than 5 mb");
        }
        
    }
    
}
