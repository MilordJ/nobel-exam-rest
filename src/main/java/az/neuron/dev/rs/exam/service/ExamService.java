/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.service;

import az.neuron.dev.rs.exam.dao.ExamDao;
import az.neuron.dev.rs.exam.dao.QuestionsDao;
import az.neuron.dev.rs.exam.dao.TicketDao;
import az.neuron.dev.rs.exam.domain.DictionaryWrapper;
import az.neuron.dev.rs.exam.domain.Exam;
import az.neuron.dev.rs.exam.domain.ExamFinishedResult;
import az.neuron.dev.rs.exam.domain.ExamResult;
import az.neuron.dev.rs.exam.domain.FileWrapper;
import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.PelcExam;
import az.neuron.dev.rs.exam.domain.Questions;
import az.neuron.dev.rs.exam.domain.Subject;
import az.neuron.dev.rs.exam.domain.Ticket;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.form.BaseForm;
import az.neuron.dev.rs.exam.form.ExamForm;
import az.neuron.dev.rs.exam.form.ExamProcessForm;
import az.neuron.dev.rs.exam.form.QuestionChoisesForm;
import az.neuron.dev.rs.exam.form.QuestionSearchForm;
import az.neuron.dev.rs.exam.form.TicketForm;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author RUHANI ALIYEV
 */
@Service
public class ExamService implements IExamService {
    
    @Autowired
    private QuestionsDao questionsDao;
    
    @Autowired
    private ExamDao examDao;
    
    @Autowired
    private TicketDao ticketDao;

    @Override
    public List<Questions> getQuestions(QuestionSearchForm form) {
       return questionsDao.getQuestions(form);
               
    }

    @Override
    public User checkToken(String token) {
        return examDao.checkToken(token);
    }

    @Override
    public List<Subject> getSubjects(BaseForm form) {
        return examDao.getSubjects(form);
    }

    @Override
    public List<DictionaryWrapper> getTopics(int subjectId) {
        return questionsDao.getTopics(subjectId);
    }

    @Override
    public OperationResponse addQuestion(QuestionSearchForm form) {
       return questionsDao.addQuestion(form);
    }

    @Override
    public OperationResponse editQuestion(QuestionSearchForm questionform) {
       return this.questionsDao.editQuestion(questionform);
    }

    @Override
    public OperationResponse removeQuestion(String token, int id) {
        return questionsDao.removeQuestion(token, id);
    }

    @Override
    public OperationResponse getQuestionDetails(String token, int id) {
        return questionsDao.getQuestionDetails(token, id);
    }

    @Override
    public OperationResponse getQuestionChoises(String token, int id) {
        return questionsDao.getQuestionChoises(token, id);
    }

    @Override
    public FileWrapper getImageByPath(String token, String path) {
        return questionsDao.getImageByPath(path);
    }


    @Override
    public OperationResponse removeFile(String token, String path) {
        return  questionsDao.removeFile(token, path);
    }

    @Override
    public OperationResponse addImageToQuestions(QuestionChoisesForm form) {
        return this.questionsDao.addImageToQuestions(form);
    }

    @Override
    public OperationResponse addTicket(TicketForm form) {
        return ticketDao.addTicket(form);
    }

    @Override
    public List<Ticket> getTickets(TicketForm form) {
        return ticketDao.getTickets(form);
    }

    @Override
    public OperationResponse getTicketQuestions(String token, int ticketId) {
        return ticketDao.getTicketQuestions(token, ticketId);
    }

    @Override
    public OperationResponse removeTicket(String token, int ticketId) {
        return ticketDao.removeTicket(token, ticketId);
    }

    @Override
    public List<Exam> getExam(BaseForm form) {
        return examDao.getExam(form);
    }

    @Override
    public OperationResponse addExam(ExamForm form) {
        return examDao.addExam(form);
    }

    @Override
    public PelcExam getExaming(ExamForm form) {
        return examDao.getParticipantExam(form);
    }

    @Override
    public OperationResponse confirmExamCode(ExamForm form) {
        return examDao.confirmExamCode(form);
    }

    @Override
    public OperationResponse editExamResult(ExamProcessForm form) {
        return examDao.editExamResult(form);
    }

    @Override
    public OperationResponse editExamDuration(ExamProcessForm form) {
        return examDao.editExamDuration(form);
    }

    @Override
    public OperationResponse getExamFinishedResult(ExamProcessForm form) {
        return examDao.getExamFinishedResult(form);
    }

    @Override
    public OperationResponse getParticipantExamList(String token) {
        return examDao.getParticipantExamList(token);
    }

    @Override
    public List<Exam> getExamList(ExamForm form) {
        return examDao.getExamList(form);
    }

    @Override
    public OperationResponse editExam(ExamForm form) {
        return examDao.editExam(form);
    }

    @Override
    public OperationResponse removeExam(String token, int id) {
        return examDao.removeExam(token, id);
    }

    @Override
    public Exam getExamListDetails(String token, int id) {
        return examDao.getExamListDetails(token, id);
    }

    @Override
    public List<ExamFinishedResult> getPartFinishedExamList(String token, int courseId) {
       return examDao.getPartFinishedExamList(token, courseId);
    }

    @Override
    public OperationResponse examParticipantTimeDetails(ExamProcessForm form) {
        return examDao.examParticipantTimeDetails(form);
    }

    @Override
    public List<ExamResult> getExamResult(String token, int examParticipantId) {
        return examDao.getExamResult(token, examParticipantId);
    }
    
    @Override
    public OperationResponse getAllFinishedExamsList(ExamForm form) {
        return examDao.getAllFinishedExamsList(form);
    }
    @Override
    public OperationResponse getParticipantFinishedExamsList(String token) {
        return examDao.getParticipantFinishedExamsList(token);
    }

    @Override
    public OperationResponse reminderMessage(int examId) {
        return examDao.reminderMessage(examId);
    }
    
}
