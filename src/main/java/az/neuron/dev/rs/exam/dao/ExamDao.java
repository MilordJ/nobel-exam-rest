/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.dao;

import az.neuron.dev.rs.exam.db.DbConnect;
import az.neuron.dev.rs.exam.domain.DictionaryWrapper;
import az.neuron.dev.rs.exam.domain.Exam;
import az.neuron.dev.rs.exam.domain.ExamFinishedResult;
import az.neuron.dev.rs.exam.domain.ExamResult;
import az.neuron.dev.rs.exam.domain.MultilanguageString;
import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.PelcExam;
import az.neuron.dev.rs.exam.domain.QuestionChoises;
import az.neuron.dev.rs.exam.domain.Questions;
import az.neuron.dev.rs.exam.domain.Subject;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.domain.UserAccount;
import az.neuron.dev.rs.exam.enums.ResultCode;
import az.neuron.dev.rs.exam.form.BaseForm;
import az.neuron.dev.rs.exam.form.ExamForm;
import az.neuron.dev.rs.exam.form.ExamProcessForm;
import az.neuron.dev.rs.exam.util.ErrorMessage;
import az.neuron.dev.rs.exam.util.MailService;
import az.neuron.dev.rs.exam.util.RowFetcher;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import static java.nio.charset.StandardCharsets.*;

/**
 *
 * @author RUHANI ALIYEV
 */
@Repository
public class ExamDao implements IExamDao {

    private static final Logger log = Logger.getLogger(ExamDao.class);

    @Autowired
    private DbConnect dbConnect;

    @Autowired
    private QuestionsDao questionsDao;

    @Override
    public User checkToken(String token) {
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{? = call ub_security.pkg_security.check_token(?)}")) {
                callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
                callableStatement.setString(2, token);
                callableStatement.execute();

                try (ResultSet resultSet = (ResultSet) callableStatement.getObject(1)) {
                    if (resultSet.next()) {
                        return RowFetcher.fetchUser(resultSet);
                    }
                }
            }

        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public List<Subject> getSubjects(BaseForm form) {
        List<Subject> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{? = call ub_exam.pkg_exam.get_subjects(?)}")) {
                callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
                callableStatement.setString(2, form.getToken());
                callableStatement.execute();

                try (ResultSet resultSet = (ResultSet) callableStatement.getObject(1)) {
                    while (resultSet.next()) {
                        list.add(new Subject(resultSet.getInt("id"),
                                new DictionaryWrapper(resultSet.getInt("subject_dic_id"),
                                        new MultilanguageString(resultSet.getString("subject_dic_az"),
                                                resultSet.getString("subject_dic_en"),
                                                resultSet.getString("subject_dic_ru")))
                        ));
                    }

                }

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public OperationResponse removeQuestion(String token, int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call = ub_exam.pkg_exam.remove_question(?,?)}")) {
                cl.setString(1, token);
                cl.setInt(2, id);

                cl.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse confirmExamCode(ExamForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call = ub_exam.pkg_exam.confirm_exam_code(?,?,?,?)}")) {
                cl.setString(1, form.getToken());
                cl.setInt(2, form.getPelcId());
                cl.setString(3, form.getExamCode());
                cl.setInt(4, form.getId());
                cl.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public List<Exam> getExam(BaseForm form) {
        List<Exam> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_exam(?)}")) {

                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, form.getToken());

                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    while (rs.next()) {
                        list.add(new Exam(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("course_id"),
                                        new MultilanguageString(rs.getString("name_az"),
                                                rs.getString("name_en"),
                                                rs.getString("name_ru"))),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("start_time"),
                                rs.getString("end_time")));
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public OperationResponse addExam(ExamForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        Set<UserAccount> set = new HashSet<>();
        try (OracleConnection connection = (OracleConnection) dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.add_exam(?,?,?,?,?,?,?,?,?,?,?)}")) {

                cl.setString(1, form.getToken());
//                cl.setInt(2, form.getCourseId());
                cl.setString(2, form.getExamStartDate());
                cl.setString(3, form.getExamFinishDate());
                cl.setString(4, form.getExamStartTime());
                cl.setString(5, form.getExamFinishTime());
                cl.setInt(6, form.getExamDuration());
//                cl.setInt(7, form.getExamTypeId());
                cl.setArray(7, connection.createARRAY("UB_EXAM.TP_NUMBER_TABLE", form.getParticipants()));
//                cl.setInt(9, form.getRepetition());
                cl.setInt(8, form.getResultVisibility());
                cl.setInt(9, form.getOrgId());
                cl.setInt(10, form.getPointBarrier());
                cl.registerOutParameter(11, OracleTypes.CURSOR);
                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(11)) {
                    while (rs.next()) {
                        set.add(new UserAccount(rs.getInt("PERSON_ID"),
                                rs.getString("USER_NAME"),
                                rs.getString("FULL_NAME"),
                                rs.getString("USER_MAIL")));
                        MailService m = new MailService();
                        String url = getUrl(rs.getInt("PARTICIPANT_ID"), rs.getInt("exam_ID"), connection);      
                        String text = "<html><body>" +
                                      "<p><img src=\"http://www.nobeloil.com/img/logo.jpg\" alt=\"\" width=\"84\" height=\"63\"></p>" +
                                      "<p><b>Hörmətli</b> "+rs.getString("FULL_NAME")+",</p>" +
                                      "<p>Siz “"+ form.getExamStartDate()+" "+form.getExamStartTime()+" - " + form.getExamFinishDate()+" "+form.getExamFinishTime()+"” tarixli Etika və Uyğunluq adlı təlimə dəvət olunursunuz.</p>" +
                                      "<p>Zəhmət olmasa aşağıdakı link üzərindən təlimə keçid alın.</p>" +
                                      "<p>Link: <a href='"+url+"'>Link for exam</a></p>" +
                                      "<p>Əlavə suallarınız olarsa Etika və Uyğunluğun təmin edilməsi üzrə şəxsə</p>" +
                                      "<p>(snovruzova@nobeloil.com email ünvanına yazmaqla) müraciət edin.</p>" +
                                      "<p><b>Hörmətlə,<b></p>" +
                                      "<p>Nobel Oil Services UK LTD</p>" +
                                      "<hr>" +
                                      "<p><img src=\"http://www.nobeloil.com/img/logo.jpg\" alt=\"\" width=\"84\" height=\"63\"></p>" +
                                      "<p><b>Dear</b> "+rs.getString("FULL_NAME")+",</p>" +
                                      "<p>You are kindly invited to attend the Ethics and Compliance training on “"+ form.getExamStartDate()+" "+form.getExamStartTime()+" - " + form.getExamFinishDate()+" "+form.getExamFinishTime()+"”.</p>" +
                                      "<p>Please use the following link to log in to the training session.</p>" +
                                      "<p>Link: <a href='"+url+"'>Link for exam</a></p>" +
                                      "<p>Please feel free to contact Ethics and Compliance Officer (by sending an e-mail to</p>" +
                                      "<p>snovruzova@nobeloil.com) should you have any further questions.</p>" +
                                      "<p><b>Regards,</b>,</p>" +
                                      "<p>Nobel Oil Services UK LTD</p>" +
                                      "</body></html>";
//                        String text = "<html><body><h1>Exam</h1><br><label>Exam date: - "+ form.getExamStartDate()+" "+form.getExamStartTime()+" - " + form.getExamFinishDate()+" "+form.getExamFinishTime()+"</label>"
//                                + " <br><a href='"+url+"'>Link for exam</a></body></html>";
//                        byte[] ptext = text.getBytes("ISO_8859_1"); 
//                        String value = new String(ptext, UTF_8); 
//                        String value = new String(text.getBytes("UTF-8"));
                        m.Send(text, "Exam", rs.getString("USER_MAIL"));
                    }
                operationResponse.setData(set);
                operationResponse.setCode(ResultCode.OK);
                }
            }
        } catch (SQLException e) {
            int code = e.getErrorCode();
            MultilanguageString message = ErrorMessage.message.get(code);
            if (message != null) {
                operationResponse.setMessage(message);
                log.info(message);
            }
            log.error(e.getMessage(), e);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @Override
    public OperationResponse reminderMessage(int examId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        Set<UserAccount> set = new HashSet<>();
        String query = "SELECT P.LNAME||' '||P.FNAME FULL_NAME, EP.PARTICIPANT_ID, UA.PERSON_ID, U.USER_NAME," +
                       "UA.USER_NAME USER_MAIL, ED.ID EXAM_ID, ED.START_DATE, ED.START_TIME," + 
                       "ED.END_DATE, ED.END_TIME FROM UB_EXAM.EXAM_PARTICIPANT EP " +
                       "JOIN UB_EXAM.EXAM_DETAILS ED ON ED.ID = EP.EXAM_DETAIL_ID AND " +
                       "ED.ACTIVE = 1 JOIN UB_SECURITY.USERS U ON U.ID = EP.PARTICIPANT_ID " +
                       "AND U.ACTIVE = 1 JOIN UB_SECURITY.USER_ACCOUNT UA ON UA.PERSON_ID" +
                       "= U.PERSONS_ID AND UA.ACTIVE = 1 JOIN UB_COMMON.PERSONS P ON P.ID = UA.PERSON_ID AND P.ACTIVE = 1 WHERE EP.EXAM_DETAIL_ID = ? AND " +
                       "EP.ACTIVE = 1";
        try(Connection connection =  dbConnect.getConnection()) {
            try(PreparedStatement ps = connection.prepareCall(query)){
                ps.setInt(1, examId);
                try(ResultSet rs = ps.executeQuery()){
                    while (rs.next()) {
                        set.add(new UserAccount(rs.getInt("PERSON_ID"),
                                                rs.getString("USER_NAME"),
                                                rs.getString("FULL_NAME"),
                                                rs.getString("USER_MAIL")));
                        MailService m = new MailService();
                        String url = getUrl(rs.getInt("PARTICIPANT_ID"), 
                                rs.getInt("EXAM_ID"), 
                                connection);
//                        String text = "<html><body><h1>Exam</h1><br><label>Exam date: - "
//                                + rs.getString("START_DATE") +" "+ rs.getString("START_TIME")
//                                +" - " + rs.getString("END_DATE") +" "+ rs.getString("END_TIME") 
//                                +"</label><br><a href='"+url+"'>Link for exam</a></body></html>";
//                         String text = "<html><body><h3>Xatırlatma;</h3>"
//                                 + "<p>Hörmətli "+rs.getString("FULL_NAME")+",</p>" +
//                            "<p>Siz “"+ rs.getString("START_DATE") +" "+ rs.getString("START_TIME")
//                                +" - " + rs.getString("END_DATE") +" "+ rs.getString("END_TIME") 
//                                +"” tarixli Biznes etikasi ve komplayens adlı təlimə dəvət olunursunuz.</p>" +
//                            "<p>Zəhmət olmasa aşağıdakı link üzərindən təlimə keçid alın.</p>" +
//                            "<p>Link: <a href='"+url+"'>Link for exam</a></p>" +
//                            "<p>Əlavə suallarınız olarsa Əlavə suallarınız olarsa komplayens menecer Sevinc Novruzova</p>" +
//                            "<p>ilə snovruzova@nobeloil.com email ünvanına yazmaqla müraciət edə bilərsiniz.</p>" +
//                            "<p>Hörmətlə,</p>" +
//                            "<p>Nobel Oil Services UK LTD</p></body></html>";
                        String text = "<html><body>" +
                                      "<p><img src=\"http://www.nobeloil.com/img/logo.jpg\" alt=\"\" width=\"84\" height=\"63\"></p>" +                                      
                                      "<h3>Xatırlatma;</h3>" +
                                      "<p><b>Hörmətli</b> "+rs.getString("FULL_NAME")+",</p>" +
                                      "<p>Siz “"+ rs.getString("START_DATE") +" "+ rs.getString("START_TIME") +" - " + rs.getString("END_DATE") +" "+ rs.getString("END_TIME") +"” tarixli Etika və Uyğunluq adlı təlimə dəvət olunursunuz.</p>" +
                                      "<p>Zəhmət olmasa aşağıdakı link üzərindən təlimə keçid alın.</p>" +
                                      "<p>Link: <a href='"+url+"'>Link for exam</a></p>" +
                                      "<p>Əlavə suallarınız olarsa Etika və Uyğunluğun təmin edilməsi üzrə şəxsə</p>" +
                                      "<p>(snovruzova@nobeloil.com email ünvanına yazmaqla) müraciət edin.</p>" +
                                      "<p><b>Hörmətlə,<b></p>" +
                                      "<p>Nobel Oil Services UK LTD</p>" +
                                      "<hr>" +
                                      "<p><img src=\"http://www.nobeloil.com/img/logo.jpg\" alt=\"\" width=\"84\" height=\"63\"></p>" +
                                      "<h3>Remind;</h3>" +
                                      "<p><b>Dear</b> "+rs.getString("FULL_NAME")+",</p>" +
                                      "<p>You are kindly invited to attend the Ethics and Compliance training on “"+ rs.getString("START_DATE") +" "+ rs.getString("START_TIME") +" - " + rs.getString("END_DATE") +" "+ rs.getString("END_TIME") +"”.</p>" +
                                      "<p>Please use the following link to log in to the training session.</p>" +
                                      "<p>Link: <a href='"+url+"'>Link for exam</a></p>" +
                                      "<p>Please feel free to contact Ethics and Compliance Officer (by sending an e-mail to</p>" +
                                      "<p>snovruzova@nobeloil.com) should you have any further questions.</p>" +
                                      "<p><b>Regards,</b>,</p>" +
                                      "<p>Nobel Oil Services UK LTD</p>" +
                                      "</body></html>";
                        m.Send(text, "Exam", rs.getString("USER_MAIL"));
                        
                    operationResponse.setCode(ResultCode.OK);
                    }
                    operationResponse.setData(set);
                }
            }
                
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    public String getUrl(int userId, int examId, Connection connection) {
        String query = "select SSID from (select A.SSID from UB_SECURITY.APP_SESSIONS a where A.USER_ID = ? and A.ACTIVE = 1 order by a.id desc) where rownum = 1";
        
            try (PreparedStatement cl = connection.prepareStatement(query)) {
                cl.setInt(1, userId);
                try (ResultSet rs = cl.executeQuery()) {
                    if (rs.next()) {
                        return "https://nobel.unibook.az/UnibookExam/#/Authentication?token=" + rs.getString(1) + "&id="+examId+"&plcId=0&participantId="+userId;
                        
                }
                }
        }catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
    
    @Override
    public PelcExam getParticipantExam(ExamForm form) {
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_participant_exam(?,?,?) }")) {

                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, form.getToken());
                cl.setInt(3, form.getPelcId());
                cl.setInt(4, form.getId());

                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    if (rs.next()) {

                        return new PelcExam(rs.getInt("id"),
                                rs.getInt("TICKET_ID"),
                                rs.getInt("exam_participant_id"),
                                rs.getString("START_TIME"),
                                rs.getString("END_TIME"),
                                rs.getString("START_DATE"),
                                rs.getString("END_DATE"),
                                rs.getString("EXAM_FULL_TIME"),
                                this.getExamResult(form.getToken(), rs.getInt("exam_participant_id")));
                    }

                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public List<ExamResult> getExamResult(String token, int participantId) {
        List<ExamResult> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_exam_result(?,?) }")) {

                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.setInt(3, participantId);

                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    while (rs.next()) {

                        String path = null;
                        if (rs.getString("file_path") != null && !rs.getString("file_path").trim().isEmpty()) {
                            String[] imagePath = rs.getString("file_path").split("/");
                            path = imagePath[imagePath.length - 1].split("\\.")[0];
                        }

                        list.add(new ExamResult(0,
                                new Questions(rs.getInt("QUEST_ID"),
//                                      getUserQuestionAnswerList(rs.getInt("result_id"), connection),
                                        this.getParticipantAnswers(participantId, rs.getInt("QUEST_ID"), connection),
//                                        rs.getInt("RIGHT_ANSWER_COUNT"),
                                        rs.getString("QUEST_CONTENT"),
                                        path,
                                        (List<QuestionChoises>) questionsDao.getQuestionChoisesForExaminer(token, rs.getInt("QUEST_ID"), participantId).getData()),
                                        rs.getInt("RIGHT_ANSWER_COUNT")));
                    }

                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return list;
    }

    
//    private List<Integer> getUserQuestionAnswerList(int resultId, Connection connection) {
//        
//        String query = "select a.answer_id from ub_exam.exam_result_answer a where a.exam_result_id = ? and a.active = 1";
//        List<Integer> list = new ArrayList<>(); 
//            try(PreparedStatement cl = connection.prepareStatement(query)){
//                cl.setInt(1, resultId);
//                try(ResultSet rs = cl.executeQuery()){
//                    while (rs.next()) {
//                        
//                        list.add(rs.getInt(1));
//                    }
//                }
//            } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
//        return list;
//    }
//    
    @Override
    public OperationResponse editExamResult(ExamProcessForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.edit_exam_result(?,?,?,?,?,?) }")) {

                cl.setString(1, form.getToken());
                cl.setInt(2, form.getQuestionId());
                cl.setInt(3, Integer.parseInt(form.getAnswerId()));
                cl.setInt(4, form.getExamId());
                cl.setInt(5, form.getTicketId());
                cl.setInt(6, form.getParticipantId());

                cl.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse editExamDuration(ExamProcessForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.edit_exam_duration(?,?,?,?) }")) {

                cl.setString(1, form.getToken());
                cl.setInt(2, form.getExamId());
                cl.setInt(3, form.getExamParticipantId());
                cl.setInt(4, form.getExamDuration());

                cl.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse getExamFinishedResult(ExamProcessForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        Exam exam = new Exam();
        ExamFinishedResult efr = null;
        List<ExamResult> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_exam_finished_result(?,?,?,?) }")) {

                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, form.getToken());
                cl.setInt(3, form.getParticipantId());
                cl.setInt(4, form.getExamId());
                cl.registerOutParameter(5, OracleTypes.CURSOR);
                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    if (rs.next()) {
                        efr = new ExamFinishedResult(rs.getInt("quest_count"),
                                                     rs.getInt("right_count"),
                                                     rs.getInt("false_count"),
                                                     rs.getInt("empty_count"),
                                                     rs.getString("mail_address"),
                                                     rs.getInt("PASSED") == 1,
                                                     rs.getInt("result_percent"));
//                        String result = rs.getInt("PASSED") == 1 ? "Passed" : "Failed";
                        String passAz = rs.getInt("PASSED") == 1 ? " uğurla keçdiniz. Təbrik edirik." : "keçmədiniz.";
                        String passEn = rs.getInt("PASSED") == 1 ? "Congratulations! You’ve successfully passed the" : "We are sorry to say but you’ve failed an";
                        MailService m = new MailService();
                        String text = "<html><body>" +
                                    "<p><img src=\"http://www.nobeloil.com/img/logo.jpg\" alt=\"\" width=\"84\" height=\"63\"></p>" +
                                    "<p><b>Hörmətli</b> "+rs.getString("FULL_NAME")+",</p>" +
                                    "<p>Siz “"+ rs.getString("STARTING")+" - " + rs.getString("FINISHING") +"” tarixli Etika və Uyğunluq adlı təlimə dair imtahandan </p>" +
                                    "<p>"+ passAz +"</p>" +
                                    "<p>Düzgün cavablar: "+rs.getInt("right_count") +"</p>" +
                                    "<p>Yanlış cavablar: "+rs.getInt("false_count") +"</p>" +
                                    "<p>Cavablandırılmamış suallar: "+rs.getInt("empty_count") +"</p>" +
                                    "<p>Nəticə: "+rs.getInt("result_percent") +"%</p>" +
                                    "<p>Əlavə suallarınız olarsa Etika və Uyğunluğun təmin edilməsi üzrə şəxsə</p>" +
                                    "<p>(snovruzova@nobeloil.com email ünvanına yazmaqla) müraciət edin.</p>" +
                                    "<p><b>Hörmətlə,</b></p>" +
                                    "<p>Nobel Oil Services UK LTD</p>" +
                                    "<hr>" +
                                    "<p><img src=\"http://www.nobeloil.com/img/logo.jpg\" alt=\"\" width=\"84\" height=\"63\"></p>" +
                                    "<p><b>Dear</b> "+rs.getString("FULL_NAME")+",</p>" +
                                    "<p>"+ passEn +" exam held with regard to the Ethics and Compliance training on “"+ rs.getString("STARTING")+" - " + rs.getString("FINISHING") +"”</p>" +
                                    "<p>Correct answers: "+rs.getInt("right_count") +"</p>" +
                                    "<p>False answers: "+rs.getInt("false_count") +"</p>" +
                                    "<p>Unanswered: "+rs.getInt("empty_count") +"</p>" +
                                    "<p>Your result: "+rs.getInt("result_percent") +"%</p>" +
                                    "<p>Please feel free to contact Ethics and Compliance Officer (by sending an e-mail to</p>" +
                                    "<p>snovruzova@nobeloil.com) should you have any further questions.</p>" +
                                    "<p><b>Regards,</b></p>" +
                                    "<p>Nobel Oil Services UK LTD</p>" +
                                    "</body></html>";
//                        String text = "<html><body><h1>Exam results</h1><br><label><p>Question count:"+ rs.getInt("quest_count")+"</p>" +
//                                      "<br><p>Correct answers: "+rs.getInt("right_count")+"</p>" +
//                                      "<br><p>False answers:" + rs.getInt("false_count")+"</p>" +
//                                      "<br><p>Unanswered:"+rs.getInt("empty_count")+"</p>" +
//                                      "<br><p>Your result: "+rs.getInt("result_percent")+"%</p></label>" +
//                                      "<br><h2>"+result+"</h2>" +
//                                      "<br><a href='https://nobel.unibook.az/'>Nobel Oil</a></body></html>";
                        
                        m.Send(text, "Exam", rs.getString("mail_address"));
                    }
                }
                exam.setEfr(efr);

                try (ResultSet rs = (ResultSet) cl.getObject(5)) {
                    while (rs.next()) {
                        String path = null;
                        if (rs.getString("file_path") != null && !rs.getString("file_path").trim().isEmpty()) {
                            String[] imagePath = rs.getString("file_path").split("/");
                            path = imagePath[imagePath.length - 1].split("\\.")[0];
                        }

                        list.add(new ExamResult(0,
                                new Questions(rs.getInt("QUEST_ID"),
                                        rs.getString("QUEST_CONTENT"),
                                        path,
                                        (List<QuestionChoises>) questionsDao.getQuestionChoises(form.getToken(), rs.getInt("QUEST_ID")).getData()),
                                this.getParticipantAnswers(form.getParticipantId(), rs.getInt("QUEST_ID"), connection)));
                    }
                    exam.setEr(list);
                }

                operationResponse.setData(exam);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @Override
    public List<ExamResult> getParticipantAnswers(int exPartId, int questId, Connection connection){
        List<ExamResult> list = new ArrayList<>();
        try(PreparedStatement ps = connection.prepareCall("SELECT TQ.QUESTIONS_ID, ERA.ANSWER_ID " +
                                                          "FROM UB_EXAM.EXAM_RESULT_ANSWER ERA " +
                                                          "JOIN UB_EXAM.EXAM_RESULT ER ON ER.ID = ERA.EXAM_RESULT_ID " +
                                                          "AND ER.ACTIVE = 1 JOIN UB_EXAM.TICKET_QUESTIONS TQ ON TQ.ID " +
                                                          " = ER.QUEST_ID AND TQ.ACTIVE = 1 WHERE ERA.ACTIVE = 1 AND " +
                                                          "TQ.QUESTIONS_ID = ? AND ER.EXAM_PARTICIPANT_ID = ?")){
            ps.setInt(1, questId);
            ps.setInt(2, exPartId);
            
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    list.add(new ExamResult(rs.getInt("QUESTIONS_ID"), rs.getInt("ANSWER_ID")));
                }
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }
    
    @Override
    public OperationResponse getParticipantExamList(String token) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        List<Exam> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_participant_exam_list(?) }")) {

                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    while (rs.next()) {
                        String subject = " " + rs.getString("SUBJECT_AZ");
                        String subjectText = "";
                        String[] n = subject.split(",");
                        for (int i = 0; i < n.length; i++) {
                            n[i].trim();
                        }
                        Set<String> s = new HashSet<>(Arrays.asList(n));
                        Iterator i = s.iterator();
                        while (i.hasNext()) {
                            subjectText += (i.next()) + ",";
                        }

                        list.add(new Exam(rs.getInt("id"),
                                new DictionaryWrapper(0,
                                        new MultilanguageString(subjectText.substring(1, subjectText.length() - 1), null, null)),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("start_time"),
                                rs.getString("end_time"),
                                rs.getString("DURATION_TIME"),
                                rs.getInt("PARTICIPANT_ID")));
                    }
                    operationResponse.setData(list);
                    operationResponse.setCode(ResultCode.OK);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public List<Exam> getExamList(ExamForm form) {
        List<Exam> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{? = call ub_exam.pkg_exam.get_exam_list(?,?,?)}")) {
                callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
                callableStatement.setString(2, form.getToken());
                callableStatement.setInt(3, form.getOrgId() != null ? form.getOrgId() : 0);
                callableStatement.setString(4, form.getExamDate());
                callableStatement.execute();

                try (ResultSet rs = (ResultSet) callableStatement.getObject(1)) {
                    while (rs.next()) {
                        list.add(new Exam(rs.getInt("id"),
                                new DictionaryWrapper(rs.getInt("STRUCTURE_ORG_ID"),
                                        new MultilanguageString(rs.getString("NAME_AZ"),
                                                rs.getString("NAME_EN"),
                                                rs.getString("NAME_RU"))),
                                rs.getString("start_date"),
                                rs.getString("end_date"),
                                rs.getString("start_time"),
                                rs.getString("end_time"),
                                rs.getString("DURATION_TIME"),
                                rs.getInt("point_barrier"),
                                0));
                    }

                }

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public List<ExamFinishedResult> getPartFinishedExamList(String token, int courseId) {
        List<ExamFinishedResult> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_participant_finished_exams(?,?)}")) {

                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.setInt(3, courseId);

                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    while (rs.next()) {
                        list.add(new ExamFinishedResult(new User(rs.getString("FNAME"), rs.getString("MNAME"), rs.getString("LNAME")),
                                rs.getInt("EXAM_ID"),
                                rs.getInt("EXAM_PARTICIPANT_ID"),
                                rs.getInt("PERSON_ID"),
                                rs.getInt("TICKET_QUESTIONS_COUNT"),
                                rs.getInt("CORRECT_ANSWERS"),
                                rs.getInt("FALSE_ANSWERS"),
                                rs.getInt("EMPTY_QUESTIONS_COUNT"),
                                rs.getInt("PARTICIPANT_ID"),
                                rs.getInt("PELC_ID"),
                                rs.getInt("TICKET_ID"),
                                rs.getInt("RESULT"),
                                rs.getString("START_DATE"),
                                rs.getString("END_DATE"),
                                rs.getString("START_TIME"),
                                rs.getString("END_TIME"),
                                rs.getString("P_START_TIME"),
                                rs.getString("P_FINISH_TIME")));
                    }
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public OperationResponse editExam(ExamForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (OracleConnection connection = (OracleConnection) dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.edit_exam(?,?,?,?,?,?,?,?,?,?,?,?)}")) {

                cl.setString(1, form.getToken());
                cl.setInt(2, form.getId());
                cl.setInt(3, form.getCourseId());
                cl.setString(4, form.getExamStartDate());
                cl.setString(5, form.getExamFinishDate());
                cl.setString(6, form.getExamStartTime());
                cl.setString(7, form.getExamFinishTime());
                cl.setInt(8, form.getExamDuration());
                cl.setInt(9, form.getResultVisibility());
                cl.setArray(10, connection.createARRAY("UB_EXAM.TP_NUMBER_TABLE", form.getParticipants()));
                cl.setInt(11, form.getPointBarrier());
                cl.registerOutParameter(12, OracleTypes.CURSOR);
                cl.execute();
                
                
                try (ResultSet rs = (ResultSet) cl.getObject(12)) {
                    while (rs.next()) {
                        MailService m = new MailService();
                        String url = getUrl(rs.getInt("PARTICIPANT_ID"), form.getId(), connection);
                        String text = "<html><body>" +
                                      "<p><img src=\"http://www.nobeloil.com/img/logo.jpg\" alt=\"\" width=\"84\" height=\"63\"></p>" +
                                      "<p><b>Hörmətli</b> "+rs.getString("FULL_NAME")+",</p>" +
                                      "<p>Siz “"+ form.getExamStartDate()+" "+form.getExamStartTime()+" - " + form.getExamFinishDate()+" "+form.getExamFinishTime()+"” tarixli Etika və Uyğunluq adlı təlimə dəvət olunursunuz.</p>" +
                                      "<p>Zəhmət olmasa aşağıdakı link üzərindən təlimə keçid alın.</p>" +
                                      "<p>Link: <a href='"+url+"'>Link for exam</a></p>" +
                                      "<p>Əlavə suallarınız olarsa Etika və Uyğunluğun təmin edilməsi üzrə şəxsə</p>" +
                                      "<p>(snovruzova@nobeloil.com email ünvanına yazmaqla) müraciət edin.</p>" +
                                      "<p><b>Hörmətlə,<b></p>" +
                                      "<p>Nobel Oil Services UK LTD</p>" +
                                      "<hr>" +
                                      "<p><img src=\"http://www.nobeloil.com/img/logo.jpg\" alt=\"\" width=\"84\" height=\"63\"></p>" +
                                      "<p><b>Dear</b> "+rs.getString("FULL_NAME")+",</p>" +
                                      "<p>You are kindly invited to attend the Ethics and Compliance training on “"+ form.getExamStartDate()+" "+form.getExamStartTime()+" - " + form.getExamFinishDate()+" "+form.getExamFinishTime()+"”.</p>" +
                                      "<p>Please use the following link to log in to the training session.</p>" +
                                      "<p>Link: <a href='"+url+"'>Link for exam</a></p>" +
                                      "<p>Please feel free to contact Ethics and Compliance Officer (by sending an e-mail to</p>" +
                                      "<p>snovruzova@nobeloil.com) should you have any further questions.</p>" +
                                      "<p><b>Regards,</b>,</p>" +
                                      "<p>Nobel Oil Services UK LTD</p>" +
                                      "</body></html>";
                        
                        m.Send(text, "Exam", rs.getString("USER_MAIL"));
                    }
                
                }
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (SQLException e) {
            int code = e.getErrorCode();
            MultilanguageString message = ErrorMessage.message.get(code);
            if (message != null) {
                operationResponse.setMessage(message);
                log.info(message);
            }
            log.error(e.getMessage(), e);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse removeExam(String token, int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.remove_exam(?,?)}")) {
                cl.setString(1, token);
                cl.setInt(2, id);
                cl.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (SQLException e) {
            int code = e.getErrorCode();
            MultilanguageString message = ErrorMessage.message.get(code);
            if (message != null) {
                operationResponse.setMessage(message);
                log.info(message);
            }
            log.error(e.getMessage(), e);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public Exam getExamListDetails(String token, int id) {
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_exam_list_details(?,?) }")) {

                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.setInt(3, id);

                cl.executeQuery();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    if (rs.next()) {

                        return new Exam(rs.getInt("id"),
                                        getExamParticipantList(rs.getInt("id"), connection),
                                        new DictionaryWrapper(rs.getInt("STRUCTURE_ORG_ID"),
                                                new MultilanguageString(rs.getString("NAME_AZ"),
                                                        rs.getString("NAME_EN"),
                                                        rs.getString("NAME_RU"))),
                                        rs.getString("start_date"),
                                        rs.getString("end_date"),
                                        rs.getString("start_time"),
                                        rs.getString("end_time"),
                                        rs.getString("DURATION_TIME"),
                                        rs.getInt("point_barrier"));
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
    
    private List<Integer> getExamParticipantList(int examId, Connection connection) {
        String query = "select " +
                        "P.PARTICIPANT_ID " +
                        "from UB_EXAM.EXAM_PARTICIPANT p where P.EXAM_DETAIL_ID = ? and P.ACTIVE = 1 ";
        List<Integer> list = new ArrayList<>();
            try (PreparedStatement ps = connection.prepareStatement(query)) {

                ps.setInt(1, examId);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {

                        list.add(rs.getInt(1));
                    }
                }
            }catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }
    
    @Override
    public OperationResponse examParticipantTimeDetails(ExamProcessForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.add_partc_ex_time_details(?,?,?) }")) {

                cl.setString(1, form.getToken());
                cl.setInt(2, form.getExamId());
                cl.setInt(3, form.getExamParticipantId());
//                cl.setString(4, form.getPartStartExam());

                cl.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse getAllFinishedExamsList(ExamForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        List<ExamFinishedResult> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_finished_exams_list(?,?,?,?,?,?)}")) {
                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, form.getToken());
                cl.setString(3, form.getParticipantName());
                cl.setString(4, form.getExamFinishDate());
                cl.setInt(5, form.getExamResultCode());
                cl.setInt(6, form.getPage());
                cl.setInt(7, form.getPageSize());
                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    while (rs.next()) {
                        list.add(new ExamFinishedResult(new User(rs.getString("NAME"), null, null),
                                                        rs.getInt("exam_id"), 
                                                        rs.getInt("EXAM_PARTICIPANT_ID"), 
                                                        rs.getInt("RIGHT_COUNT"), 
                                                        rs.getInt("FALSE_COUNT"), 
                                                        rs.getInt("EMPTY_COUNT"), 
                                                        rs.getString("EXAM_DATE"), 
                                                        rs.getInt("POINT_BARRIER"), 
                                                        rs.getInt("USER_PERCENT"), 
                                                        rs.getString("USER_START_DATE"), 
                                                        rs.getString("USER_END_DATE")));
                    }
                    operationResponse.setData(list);
                    operationResponse.setCode(ResultCode.OK);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse getParticipantFinishedExamsList(String token) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        List<ExamFinishedResult> list = new ArrayList<>();
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_finished_exams_by_user(?) }")) {
                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.execute();

                try (ResultSet rs = (ResultSet) cl.getObject(1)) {
                    while(rs.next()) {

                        list.add(new ExamFinishedResult(rs.getInt("id"),
                                rs.getInt("PARTICIPANT_ID"),
                                new DictionaryWrapper(0,
                                        new MultilanguageString(rs.getString("SUBJECTS"),
                                                null,
                                                null)),
                                rs.getInt("RIGHT_CHOICES_COUNT"),
                                rs.getInt("V_PERCENT"),
                                rs.getInt("QUESTION_COUNT"),
                                rs.getInt("DURATION_TIME"),
                                //                                                    rs.getString("CODE"), 
                                rs.getString("START_DATE"),
                                rs.getString("END_DATE"),
                                rs.getString("START_TIME"),
                                rs.getString("END_TIME")));
                    }
                    operationResponse.setData(list);
                    operationResponse.setCode(ResultCode.OK);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

}
