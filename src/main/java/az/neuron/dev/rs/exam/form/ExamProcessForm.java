/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.form;

import lombok.Data;

/**
 *
 * @author Lito
 */
@Data
public class ExamProcessForm extends BaseForm{
     private int id;
     private int participantId;
     private int examParticipantId;
     private int examId;
     private int examDuration;
     private int ticketId;
     private int questionId;
     private String answerId;
     private String partStartExam;
     
}
