/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.controller;

import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.enums.ResultCode;
import az.neuron.dev.rs.exam.form.BaseForm;
import az.neuron.dev.rs.exam.form.ExamForm;
import az.neuron.dev.rs.exam.form.TicketForm;
import az.neuron.dev.rs.exam.service.ExamService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 *
 * @author RUHANI ALIYEV
 */
@RestController
@RequestMapping(value = "/ticket", produces = MediaType.APPLICATION_JSON_VALUE)
public class TicketController extends SkeletonController {
    private static final Logger log = Logger.getLogger(TicketController.class);
    
  
    @Autowired
    private ExamService service;
    
    @PostMapping (value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse addTicket (
            @RequestPart(name = "question", required = false) TicketForm form, 
                                                MultipartHttpServletRequest request) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/add [POST]. Form: " + form + ", user: " + user);
                   
                   operationResponse = service.addTicket(form);
              
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping
    protected OperationResponse getTickes(TicketForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/ticket [GET].Form: " + form + ", user: " + user);
            operationResponse.setData(service.getTickets(form));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
                            
    @GetMapping(value = "/{ticketId:\\d+}")
    protected OperationResponse getTicketQuestions(BaseForm form, @PathVariable int ticketId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{ticketId:\\d+} [GET].Form: " + form + ", user: " + user);
            operationResponse = service.getTicketQuestions(form.getToken(), ticketId);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @PostMapping(value = "/{ticketId:\\d+}/remove")
    protected OperationResponse removeTicket(BaseForm form, @PathVariable int ticketId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{ticketId:\\d+}/remove [POST].Form: " + form + ", user: " + user);
            operationResponse = service.removeTicket(form.getToken(), ticketId);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
}
