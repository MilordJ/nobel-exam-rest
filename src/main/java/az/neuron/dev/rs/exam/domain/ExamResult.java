/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import java.util.List;
import lombok.Data;

/**
 *
 * @author Lito
 */
@Data
public class ExamResult {
    int id;
    int questId;
    int questCount;
    int rightCount;
    int falseCount;
    int emptyCount;
    int answerId;
    int rightAnswerCount;
    int exParticipantId;
    List<ExamResult> list;
    Questions question;

    public ExamResult() {
    }

    public ExamResult(int id, Questions question, int rightAnswerCount) {
        this.id = id;
        this.question = question;
        this.rightAnswerCount = rightAnswerCount;
//        this.answerId = answerId;
    }
    public ExamResult(int id, Questions question, List<ExamResult> list) {
        this.id = id;
        this.question = question;
        this.list = list;
    }

    public ExamResult(int questId, int answerId) {
        this.questId = questId;
        this.answerId = answerId;
    }

    public ExamResult(int id, int exParticipantId, Questions question) {
        this.id = id;
        this.exParticipantId = exParticipantId;
        this.question = question;
    }
    
    
}
