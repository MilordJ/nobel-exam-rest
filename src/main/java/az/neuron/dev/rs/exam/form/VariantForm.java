/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.form;

import lombok.Data;

/**
 *
 * @author RUHANI ALIYEV
 */
@Data
public class VariantForm {
    private String content;
    private FileWrapperForm fileForm;
    private Boolean rightChoise;
    private int id;
    
}
