/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.form;

import az.neuron.dev.rs.exam.domain.FileWrapper;
import lombok.Data;

/**
 *
 * @author RUHANI ALIYEV
 */

@Data
public class QuestionChoisesForm  extends BaseForm{
    
    private int id;
    private int questId;
    private String questionContent;
    private int rightChoise;
    private FileWrapperForm fileForm;

}
