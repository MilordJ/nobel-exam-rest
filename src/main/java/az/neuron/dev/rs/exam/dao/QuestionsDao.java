/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.dao;

import az.neuron.dev.rs.exam.db.DbConnect;
import az.neuron.dev.rs.exam.domain.DictionaryWrapper;
import az.neuron.dev.rs.exam.domain.FileWrapper;
import az.neuron.dev.rs.exam.domain.MultilanguageString;
import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.QuestionChoises;
import az.neuron.dev.rs.exam.domain.Questions;
import az.neuron.dev.rs.exam.enums.ResultCode;
import az.neuron.dev.rs.exam.form.FileWrapperForm;
import az.neuron.dev.rs.exam.form.QuestionChoisesForm;
import az.neuron.dev.rs.exam.form.QuestionSearchForm;
import az.neuron.dev.rs.exam.form.VariantForm;
import az.neuron.dev.rs.exam.util.Converter;
import az.neuron.dev.rs.exam.util.ErrorMessage;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author RUHANI ALIYEV
 */
@Repository
public class QuestionsDao implements IQuestionsDao {

    private static final Logger log = Logger.getLogger(ExamDao.class);

    @Autowired
    private DbConnect dbConnect;

    @Override
    public List<Questions> getQuestions(QuestionSearchForm form) {
        List<Questions> list = new ArrayList<>();

        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{? = call ub_exam.pkg_exam.get_questions(?,?,?,?,?,?,?,?,?,?,?) }")) {
                callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
                callableStatement.setString(2, form.getToken());
                callableStatement.setInt(3, form.getSubjectId() == null ? 0 : form.getSubjectId());
                callableStatement.setInt(4, form.getQuestionLevelId() == null ? 0 : form.getQuestionLevelId());
                callableStatement.setInt(5, form.getQuestionTypeId() == null ? 0 : form.getQuestionTypeId());
                callableStatement.setInt(6, form.getEduLevelId() == null ? 0 : form.getEduLevelId());
                callableStatement.setInt(7, form.getEduPlanId()== null ? 0 : form.getEduPlanId());
                callableStatement.setInt(8, form.getTipiId() == null ? 0 : form.getTipiId());
                callableStatement.setInt(9, form.getLangId() == null ? 0 : form.getLangId());
                callableStatement.setString(10, form.getKeyword());
                callableStatement.setInt(11, form.getPageSize());
                callableStatement.setInt(12, form.getPage());   
                callableStatement.execute();

                try (ResultSet resultSet = (ResultSet) callableStatement.getObject(1)) {
                    while (resultSet.next()) {
                        list.add(new Questions(resultSet.getInt("id"),
                                                resultSet.getString("quest_content"),
                                                new DictionaryWrapper(resultSet.getInt("subject_id"), 
                                                        new MultilanguageString(resultSet.getString("subject_az"), 
                                                                                resultSet.getString("subject_en"), 
                                                                                resultSet.getString("subject_ru"))),
                                                new DictionaryWrapper(resultSet.getInt("quest_level_id"), 
                                                        new MultilanguageString(resultSet.getString("quest_level_az"), 
                                                                                resultSet.getString("quest_level_en"), 
                                                                                resultSet.getString("quest_level_ru"))),    
                                                new DictionaryWrapper(resultSet.getInt("quest_type_id"), 
                                                        new MultilanguageString(resultSet.getString("quest_type_az"), 
                                                                                resultSet.getString("quest_type_en"), 
                                                                                resultSet.getString("quest_type_ru"))),
                                                new DictionaryWrapper(resultSet.getInt("quest_topic_id"), 
                                                        new MultilanguageString(resultSet.getString("quest_topic_az"), 
                                                                                resultSet.getString("quest_topic_en"), 
                                                                                resultSet.getString("quest_topic_ru")))
                                            ));
                                        }
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public List<DictionaryWrapper> getTopics(int subjectId) {
        List<DictionaryWrapper> list = null;

        try (Connection connection = dbConnect.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareCall(" select * from ub_exam.v_topic where id = ? ")) {
                preparedStatement.setInt(1, subjectId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    list = new ArrayList<>();
                    while (resultSet.next()) {
                        list.add(new DictionaryWrapper(resultSet.getInt("id"),
                                new MultilanguageString(resultSet.getString("topic_name"), null, null)));
                    }
                }

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }
    
    @Override
    public OperationResponse updateQuestionChoises(QuestionChoisesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        int fileId = 0;
        if (form.getFileForm()!= null && !form.getFileForm().getFile().isEmpty()) {
            fileId = this.addFile(form.getToken(), form.getFileForm());
        }
        try(Connection connection = dbConnect.getConnection()) {
            try(CallableStatement cl = connection.prepareCall("{ call ub_exam.pkg_exam.edit_quest_choise(?,?,?,?,?,?) }")){
                
                cl.setString(1, form.getToken());
                cl.setInt(2, form.getQuestId());
                cl.setInt(3, form.getId());
                cl.setString(4, form.getQuestionContent());
                cl.setInt(5, fileId);
                cl.setInt(6, form.getRightChoise());
                
                cl.executeUpdate();
                
                operationResponse.setCode(ResultCode.OK);
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @Override
    public OperationResponse addQuestion(QuestionSearchForm questionform) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        int fileId = 0;
        if (questionform.getFileForm() != null && !questionform.getFileForm().getFile().isEmpty()) {
            fileId = this.addFile(questionform.getToken(), questionform.getFileForm());
        }

        if (questionform.getVariants().length > 0) {
            int varinatFileId = 0;
            for (VariantForm variant : questionform.getVariants()) {
                if (variant.getFileForm() != null) {
                    varinatFileId = this.addFile(questionform.getToken(), variant.getFileForm());
                    variant.getFileForm().setId(varinatFileId);
                }
            }
        }

        try (OracleConnection connection = (OracleConnection) dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call ub_exam.pkg_exam.add_questions(?,?,?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, questionform.getToken());
                callableStatement.setInt(2, questionform.getTopicId() == null ? 0 : questionform.getTopicId());
                callableStatement.setInt(3, questionform.getQuestionLevelId() == null ? 0 : questionform.getQuestionLevelId());
//                callableStatement.setInt(4, questionform.getQuestionTypeId() == null ? 0 : questionform.getQuestionTypeId());
                callableStatement.setInt(4, 1011743);
                callableStatement.setInt(5, fileId);
//                callableStatement.setInt(6, questionform.getEduLevelId() == null ? 0 : questionform.getEduLevelId());
//                callableStatement.setInt(7, questionform.getEduPlanId()== null ? 0 : questionform.getEduPlanId());
                callableStatement.setInt(6, questionform.getLangId() == null ? 0 : questionform.getLangId());
//                callableStatement.setInt(9, questionform.getTipiId()== null ? 0 : questionform.getTipiId());
                callableStatement.setInt(7, questionform.getSubjectId() == null ? 0 : questionform.getSubjectId());
                callableStatement.setString(8, questionform.getContent());
                callableStatement.setArray(9, connection.createARRAY("UB_EXAM.TP_QUEST_CHOISE_TABLE", questionform.getVariants() == null ? new Object[][]{} : Converter.toOracleVariantObject(questionform.getVariants())));
                callableStatement.executeUpdate();
                
                operationResponse.setCode(ResultCode.OK);
            }

        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;
    }
    
    @Override
    public OperationResponse editQuestion(QuestionSearchForm questionform) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (OracleConnection connection = (OracleConnection) dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call ub_exam.pkg_exam.edit_questions(?,?,?,?,?,?,?,?,?)}")) {
                callableStatement.setString(1, questionform.getToken());
                callableStatement.setInt(2, questionform.getTopicId() == null ? 0 : questionform.getTopicId());
                callableStatement.setInt(3, questionform.getQuestionLevelId() == null ? 0 : questionform.getQuestionLevelId());
                callableStatement.setInt(4, 1011743);
//                callableStatement.setInt(4, questionform.getQuestionTypeId() == null ? 0 : questionform.getQuestionTypeId());
//                callableStatement.setInt(5, questionform.getEduLevelId() == null ? 0 : questionform.getEduLevelId());
                callableStatement.setInt(5, questionform.getLangId() == null ? 0 : questionform.getLangId());
//                callableStatement.setInt(7, questionform.getTipiId()== null ? 0 : questionform.getTipiId());
                callableStatement.setInt(6, questionform.getSubjectId() == null ? 0 : questionform.getSubjectId());
                callableStatement.setString(7, questionform.getContent());
                callableStatement.setInt(8, questionform.getId());
                callableStatement.setArray(9, connection.createARRAY("UB_EXAM.TP_QUEST_CHOISE_TABLE", questionform.getVariants() == null ? new Object[][]{} : Converter.toOracleVariantObject(questionform.getVariants())));
//                callableStatement.setInt(12, questionform.getEduPlanId() == null ? 0 : questionform.getEduPlanId());
                callableStatement.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
            }

        }catch(SQLException exception){
            int code = exception.getErrorCode();
            MultilanguageString ms = ErrorMessage.message.get(code);
            if(ms!=null){
                operationResponse.setMessage(ms);
                log.info(ms);
            }
            log.error(exception.getMessage(), exception);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        
        return operationResponse;

    }

    @Override
    public int addFile(String token, FileWrapperForm fileForm) {
        int id = 0;
        String fileMb = Long.toString(fileForm.getFile().getSize());
        try (OracleConnection connection = (OracleConnection) dbConnect.getConnection()) {
            try (CallableStatement callableStatement = connection.prepareCall("{ call ub_exam.pkg_exam.add_questions_file(?,?,?,?,?,?)}")) {
                callableStatement.setString(1, token);
                callableStatement.setString(2, fileForm.getFile().getOriginalFilename());
                callableStatement.setString(3, fileForm.getPath());
                callableStatement.setString(4, fileMb);
                callableStatement.setString(5, fileForm.getFile().getContentType());
                callableStatement.registerOutParameter(6, Types.INTEGER);
                callableStatement.executeUpdate();

                id = callableStatement.getInt(6);

            }

        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return id;
    }

    @Override
    public OperationResponse removeQuestion(String token, int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (Connection connection = dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{ call ub_exam.pkg_exam.remove_question(?,?)}")) {
                cl.setString(1, token);
                cl.setInt(2, id);

                cl.executeUpdate();

                operationResponse.setCode(ResultCode.OK);
            }
        } catch (SQLException exception) {
            int code = exception.getErrorCode();
            MultilanguageString ms = ErrorMessage.message.get(code);
            if (ms != null) {
                operationResponse.setMessage(ms);
                log.info(ms);
            }
            log.error(exception.getMessage(), exception);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse getQuestionDetails(String token, int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        Questions questions = null; 
        try(Connection connection = dbConnect.getConnection()) {
            try(CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_question_details(?,?) }")){
                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.setInt(3, id);
                
                cl.execute();
                
                try(ResultSet rs = (ResultSet) cl.getObject(1)) {
                    if (rs.next()) {
//                        String filePath = rs.getString("file_path");
                        String path = null;
                        if (rs.getString("file_path") != null && !rs.getString("file_path").trim().isEmpty()) {
                            String[] imagePath = rs.getString("file_path").split("/");
                            path = imagePath[imagePath.length - 1].split("\\.")[0];
                        }
                        
                            OperationResponse optionOperationResponse = this.getQuestionChoises(token, id);
                        List<QuestionChoises> optionList = (List<QuestionChoises>) optionOperationResponse.getData();
                        questions = new Questions(rs.getInt("id"), 
                                                  rs.getString("QUEST_CONTENT"),
                                                    new DictionaryWrapper(rs.getInt("SUBJECT_ID"), 
                                                        new MultilanguageString(rs.getString("SUBJECT_az"), 
                                                                                rs.getString("SUBJECT_en"), 
                                                                                rs.getString("SUBJECT_ru"))), 
                                                    new DictionaryWrapper(rs.getInt("QUEST_LEVEL_ID"), 
                                                        new MultilanguageString(rs.getString("LEVEL_az"), 
                                                                                rs.getString("LEVEL_en"), 
                                                                                rs.getString("LEVEL_ru"))), 
                                                    new DictionaryWrapper(rs.getInt("QUEST_TYPE_ID"), 
                                                        new MultilanguageString(rs.getString("TYPE_az"), 
                                                                                rs.getString("TYPE_en"), 
                                                                                rs.getString("TYPE_ru"))), 
                                                    new DictionaryWrapper(rs.getInt("QUEST_TOPIC_ID"),
                                                        new MultilanguageString(rs.getString("QUEST_TOPIC_AZ"),
                                                                                rs.getString("QUEST_TOPIC_EN"),
                                                                                rs.getString("QUEST_TOPIC_RU"))), 
                                                    new DictionaryWrapper(rs.getInt("EDU_LANG_ID"), 
                                                        new MultilanguageString(rs.getString("LANG_az"), 
                                                                                rs.getString("LANG_en"), 
                                                                                rs.getString("LANG_ru"))),
                                                    new FileWrapper(rs.getInt("photo_file_id"),
                                                                    rs.getString("ORIGINAL_NAME"),
                                                                    path),
                                                    optionList);

                    }
                }
                operationResponse.setData(questions);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse getQuestionChoises(String token, int id) {
        List<QuestionChoises> list = new ArrayList<>(); 
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try(Connection connection = dbConnect.getConnection()) {
            try(CallableStatement cl = connection.prepareCall("{ ? = call ub_exam.pkg_exam.get_question_choises(?,?) }")){
                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.setInt(3, id);

                cl.execute();
                try(ResultSet rs = (ResultSet) cl.getObject(1)){
                    while (rs.next()) {
                        String path = null;
                        if (rs.getString("file_path") != null && !rs.getString("file_path").trim().isEmpty()) {
                            String[] imagePath = rs.getString("file_path").split("/");
                            path = imagePath[imagePath.length - 1].split("\\.")[0];
                        }
                        
                        list.add( new QuestionChoises(
                                                rs.getInt("id"),
                                                rs.getInt("QUEST_ID"),
                                                rs.getString("QUEST_CH_CONTENT"),
                                                rs.getInt("RIGHT_CHOISE"),
                                                new FileWrapper(path)));
                    }
                }
                operationResponse.setData(list);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse getQuestionChoisesForExaminer(String token, int questId, int participantId) {
        List<QuestionChoises> list = new ArrayList<>(); 
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try(Connection connection = dbConnect.getConnection()) {
            try(CallableStatement cl = connection.prepareCall("{ ? = call ub_exam.pkg_exam.get_question_choises_examiner(?,?,?) }")){
                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.setInt(3, questId);
                cl.setInt(4, participantId);

                cl.execute();
                try(ResultSet rs = (ResultSet) cl.getObject(1)){
                    while (rs.next()) {
                        String path = null;
                        if (rs.getString("file_path") != null && !rs.getString("file_path").trim().isEmpty()) {
                            String[] imagePath = rs.getString("file_path").split("/");
                            path = imagePath[imagePath.length - 1].split("\\.")[0];
                        }
                        
                        list.add( new QuestionChoises(
                                                rs.getInt("id"),
                                                rs.getInt("QUEST_ID"),
                                                rs.getString("QUEST_CH_CONTENT"),
                                                new FileWrapper(path)));
                    }
                }
                operationResponse.setData(list);
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @Override
    public FileWrapper getImageByPath(String path) {
        try(Connection connection = dbConnect.getConnection()) {
            try(PreparedStatement ps = connection.prepareCall("select * from ub_exam.question_files f where f.file_path like '%'||?||'%'")){
                ps.setString(1, path);
                
                try(ResultSet rs = ps.executeQuery()){
                    if (rs.next()) {
                        return new FileWrapper(rs.getInt("id"),
                                               rs.getString("original_name"),
                                               rs.getString("file_path"));
                    }
                }
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public OperationResponse removeFile(String token, String path) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try(Connection connection = dbConnect.getConnection()) {
            try(CallableStatement cl = connection.prepareCall("{ call ub_exam.pkg_exam.remove_questions_file(?,?) }")){
                
                cl.setString(1, token);
                cl.setString(2, path);
                cl.executeUpdate();
                
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse updateQuestion(int id, QuestionSearchForm form) {
        
        int fileId = 0;
        if (form.getFileForm() != null && !form.getFileForm().getFile().isEmpty()) {
            fileId = this.addFile(form.getToken(), form.getFileForm());
        }

        if (form.getVariants().length > 0) {
            int varinatFileId = 0;
            for (VariantForm variant : form.getVariants()) {
                if (variant.getFileForm() != null) {
                    varinatFileId = this.addFile(form.getToken(), variant.getFileForm());
                    variant.getFileForm().setId(varinatFileId);
                }
            }

        }
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try(Connection connection = dbConnect.getConnection()){
            try(CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.update_question(?,?,?,?,?,?,?,?,?,?)}")){
                
                cl.setString(1, form.getToken());
                cl.setInt(2, form.getTopicId());
                cl.setInt(3, form.getQuestionLevelId());
                cl.setInt(4, fileId);
                cl.setInt(5, form.getEduLevelId());
                cl.setInt(6, form.getLangId());
                cl.setInt(7, form.getTipiId());
                cl.setInt(8, form.getSubjectId());
                cl.setString(9, form.getContent());
                cl.setInt(10, form.getId());
                
                cl.executeUpdate();
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse addImageToQuestions(QuestionChoisesForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try(Connection connection = dbConnect.getConnection()) {
            String fileMb = Long.toString(form.getFileForm().getFile().getSize());
            try(CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.add_image_file_to_question(?,?,?,?,?,?,?) }")){
                
                cl.setString(1, form.getToken());
                cl.setInt(2, form.getQuestId());
                cl.setInt(3, form.getId());
                cl.setString(4, form.getFileForm().getFile().getOriginalFilename());
                cl.setString(5, form.getFileForm().getPath());
                cl.setString(6, fileMb);
                cl.setString(7, form.getFileForm().getFile().getContentType());
                cl.execute();
                
                operationResponse.setCode(ResultCode.OK);
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

}
