/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.hsis.util;

import java.io.UnsupportedEncodingException;
import java.util.UUID;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Nazrin
 */
public class Crypto {
    public static final String getGuid() {
        UUID uuid = UUID.randomUUID();
        String guid = uuid.toString().replaceAll("-", "");
        return guid;
    }
    
    public static final String getDoubleGuid() {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        String guid = uuid1.toString().replaceAll("-", "") + uuid2.toString().replaceAll("-", "");
        return guid;
    }
    
    public static final String encodeBase64(String data) throws UnsupportedEncodingException {
        byte[] bs = Base64.encodeBase64(data.getBytes());
        return new String(bs, "UTF-8");
    }
    
    public static final String decodeBase64(String data) throws UnsupportedEncodingException {
        byte[] bs = Base64.decodeBase64(data);
        return new String(bs, "UTF-8");
    }
}
