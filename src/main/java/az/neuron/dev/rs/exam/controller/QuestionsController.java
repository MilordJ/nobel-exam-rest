/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.controller;

import az.neuron.dev.rs.exam.domain.FileWrapper;
import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.enums.Constants;
import az.neuron.dev.rs.exam.enums.Regex;
import az.neuron.dev.rs.exam.enums.ResultCode;
import az.neuron.dev.rs.exam.exception.InvalidParametersException;
import az.neuron.dev.rs.exam.form.BaseForm;
import az.neuron.dev.rs.exam.form.FileWrapperForm;
import az.neuron.dev.rs.exam.form.QuestionChoisesForm;
import az.neuron.dev.rs.exam.form.QuestionSearchForm;
import az.neuron.dev.rs.exam.form.VariantForm;
import az.neuron.dev.rs.exam.service.ExamService;
import az.neuron.dev.rs.exam.util.FtpUtils;
import az.neuron.dev.rs.exam.validator.FileWrapperFormValidator;
import az.neuron.dev.rs.exam.validator.QuestionFormValidator;
import az.neuron.dev.rs.exam.validator.VariantFormValidator;
import az.neuron.dev.rs.hsis.util.Crypto;
import java.io.Console;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 *
 * @author RUHANI ALIYEV
 */
@RestController
@RequestMapping(value = "/questions", produces = MediaType.APPLICATION_JSON_VALUE)
public class QuestionsController extends SkeletonController {

    private static final Logger log = Logger.getLogger(QuestionsController.class);

    @Autowired
    private ExamService service;

    @GetMapping
    protected OperationResponse getQuestions(QuestionSearchForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/questions [GET].Form: " + form + ", user: " + user);
            operationResponse.setData(service.getQuestions(form));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @GetMapping(value = "/{questId:\\d+}/choises")
    protected OperationResponse getQuestionChoises(BaseForm form, @PathVariable int questId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{questId:\\d+}/choises [GET].Form: " + form + ", user: " + user);
            operationResponse = service.getQuestionChoises(form.getToken(), questId);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @GetMapping(value = "/file/{path}")
    protected byte[] getImageByFileId(BaseForm form, 
                                          @PathVariable String path, 
                                          HttpServletResponse response) {

        try {
            User user = checkToken(form.getToken());
            log.info("/file/{path} [GET].Form: " + form + ", user: " + user);
            
            FileWrapper file = service.getImageByPath(form.getToken(), path);
            
            if (file != null && file.getOriginalName() != null) {
                String[] x = file.getOriginalName().split("\\.");
                response.setHeader("Content-Type", FtpUtils.getType(x[x.length - 1].toLowerCase()));
                response.setHeader("Content-Disposition", "attachment; filename=" + file.getOriginalName());
                return ftpService.downloadFtpFile(file.getPath());
                    
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
    
    @GetMapping(value = "/{id:\\d+}")
    protected OperationResponse getQuestionDetails(BaseForm form, @PathVariable int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{id:\\d+} [GET].Form: " + form + ", user: " + user);
            operationResponse = service.getQuestionDetails(form.getToken(), id);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @PostMapping(value = "/file/{path}/remove")
    protected OperationResponse removeFile(BaseForm form, 
                                            @PathVariable String path) {
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        
        try {
            User user  = checkToken(operationResponse, form.getToken());
            log.info("/file/{path}/remove [POST].Form: " + form +", user: " + user);

            FileWrapper questFile = service.getImageByPath(form.getToken(), path);
            operationResponse = ftpService.removeFtpFile(questFile.getPath());
            if (operationResponse.getCode() == ResultCode.OK) {
                operationResponse = service.removeFile(form.getToken(), path);
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/remove")
    protected OperationResponse removeQuestion(BaseForm form, @PathVariable int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{id:\\d+}/remove [POST].Form: " + form + ", user: " + user);
            operationResponse = service.removeQuestion(form.getToken(), id);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }

    @GetMapping("/topics/subject/{id:\\d+}")
    protected OperationResponse getTopics(BaseForm form,
            @PathVariable("id") int subjectId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            operationResponse.setData(service.getTopics(subjectId));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }

    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse addQuestions(@RequestPart(name = "question", required = false) QuestionSearchForm form,
            @RequestPart(name = "file", required = false) MultipartFile file,
            MultipartHttpServletRequest multipartHttpServletRequest,
            BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("questions/add [POST]. Form: " + form + ", user: " + user);

            QuestionFormValidator questionValidator = new QuestionFormValidator(Constants.VALIDATOR_ADD);
            questionValidator.validate(form, result);
            if (result.hasErrors()) {
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                throw new InvalidParametersException("Invalid question form params. Form:  " + form + ", invalid params: " + result.getAllErrors());
            }

            if (file != null && file.getSize() > 0) {
                if (!file.getContentType().matches(Regex.IMAGE_CONTENT_TYPE)) {
                    throw new InvalidParametersException("Invalid file content type ");
                }
                OperationResponse saveFileResponse;
                saveFileResponse = ftpService.saveFtpFile(rootDirectory + "/question_files", file, Crypto.getGuid());

                if (saveFileResponse.getCode() != ResultCode.OK) {
                    throw new InvalidParametersException("File doesn't saved! Form code: " + saveFileResponse.getCode());
                }
                FileWrapperForm fileForm = new FileWrapperForm((String) saveFileResponse.getData(), file);
                form.setFileForm(fileForm);

            }

            Map<String, MultipartFile> requestParts = multipartHttpServletRequest.getFileMap();
            VariantForm[] varinats = form.getVariants();
            MultipartFile variantFile;
            if (varinats.length > 0) {
                for (VariantForm variant : varinats) {
//                    VariantFormValidator variantValidator = new VariantFormValidator(Constants.VALIDATOR_ADD);
//                    variantValidator.validate(variant, result);
//                    if (result.hasErrors()) {
//                        operationResponse.setData(result.getAllErrors());
//                        operationResponse.setCode(ResultCode.INVALID_PARAMS);
//                        throw new InvalidParametersException("Invalid variant form params. Form:  " + form + ", invalid params: " + result.getAllErrors());
//                    }
                    variantFile = requestParts.get("file_" + variant.getId());
                    if(varinats == null && variantFile == null){
                        throw new InvalidParametersException("Invalid variant form params. Form:  " + form + ", invalid params: " + result.getAllErrors());
                    }
                    if (variantFile != null && variantFile.getSize() > 0) {
                        if (!variantFile.getContentType().matches(Regex.IMAGE_CONTENT_TYPE)) {
                            throw new InvalidParametersException("Invalid file content type ");
                        }
                        OperationResponse saveFileResponse;
                        saveFileResponse = ftpService.saveFtpFile(rootDirectory + "/question_files", variantFile, Crypto.getGuid());

                        if (saveFileResponse.getCode() != ResultCode.OK) {
                           throw new InvalidParametersException("File doesn't saved! Form code: " + saveFileResponse.getCode());
                        }
                        FileWrapperForm fileForm = new FileWrapperForm((String) saveFileResponse.getData(), variantFile);
                        variant.setFileForm(fileForm);
                    }
                }
            }

            operationResponse = service.addQuestion(form);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @PostMapping(value = "/{id:\\d+}/update", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse updateQuestions(
            @PathVariable int id, 
            @RequestPart(name = "question", required = false) QuestionSearchForm form,
            BindingResult result) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{id:\\d+}/update [POST]. Form: " + form + ", user: " + user);
            form.setId(id);
            QuestionFormValidator questionValidator = new QuestionFormValidator(Constants.VALIDATOR_EDIT);
            questionValidator.validate(form, result);
            if (result.hasErrors()) {
                operationResponse.setData(result.getAllErrors());
                operationResponse.setCode(ResultCode.INVALID_PARAMS);
                throw new InvalidParametersException("Invalid question form params. Form:  " + form + ", invalid params: " + result.getAllErrors());
            }

            VariantForm[] varinats = form.getVariants();
            if (varinats.length > 0) {
                for (VariantForm variant : varinats) {
                    VariantFormValidator variantValidator = new VariantFormValidator(Constants.VALIDATOR_EDIT);
                    variantValidator.validate(variant, result);
                    if (result.hasErrors()) {
                        operationResponse.setData(result.getAllErrors());
                        operationResponse.setCode(ResultCode.INVALID_PARAMS);
                        throw new InvalidParametersException("Invalid variant form params. Form:  " + form + ", invalid params: " + result.getAllErrors());
                    }
                }
            }

            operationResponse = service.editQuestion(form);
//            service.editQuestion(form);
//            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @PostMapping (value = "/{questId:\\d+}/add",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    protected OperationResponse addImageToQuestion (
            @PathVariable int questId,
            @RequestPart(name = "questionDetails", required = false) QuestionChoisesForm form, 
            @RequestPart(name="file", required = false) MultipartFile file,
                                                MultipartHttpServletRequest request) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/question/{questId:\\d+}/add [POST]. Form: " + form + ", user: " + user);
            form.setQuestId(questId);
                   if (file != null && file.getSize() > 0) {
                       if (!file.getContentType().matches(Regex.IMAGE_CONTENT_TYPE)) {
                           throw new InvalidParametersException("Invalid file content type ");
                       }
                       OperationResponse saveFileResponse;
                       saveFileResponse = ftpService.saveFtpFile(rootDirectory + "/question_files", file, Crypto.getGuid());

                       if (saveFileResponse.getCode() == ResultCode.OK) {
                           FileWrapperForm fileForm = new FileWrapperForm((String) saveFileResponse.getData(), file);
                           form.setFileForm(fileForm);   
                       }
                   }
              
               operationResponse = service.addImageToQuestions(form);
        
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

}
