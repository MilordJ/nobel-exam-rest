/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import lombok.Data;

/**
 *
 * @author Nazrin
 */

@Data
public class UserAccount {
    private int id;
    private String username;
    private String fullName;
    private String mailAddress;
    private String password;
    private DictionaryWrapper role;
    private boolean blocked;

    public UserAccount() {
    }

    public UserAccount(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public UserAccount(int id, String username, DictionaryWrapper role) {
        this.id = id;
        this.username = username;
        this.role = role;
    }
    
    public UserAccount(int id, String username, String fullName, String password) {
        this.id = id;
        this.username = username;
        this.fullName = fullName;
        this.password = password;
    }

    public UserAccount(int id, String username, String password, boolean blocked) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.blocked = blocked;
    }

    public UserAccount(int id, String username, String password, DictionaryWrapper role, boolean blocked) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.blocked = blocked;
    }
    
}
