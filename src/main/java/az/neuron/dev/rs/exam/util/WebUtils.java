/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.hsis.util;

import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.servlet.support.RequestContextUtils;

/**
 *
 * @author Nazrin
 */
public class WebUtils {
    public static Locale getLocale(HttpServletRequest request) {
        return RequestContextUtils.getLocale(request);
    }
    
    public static String getIpAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");  
        if (ipAddress == null) {  
           ipAddress = request.getRemoteAddr();  
        }
        
        return ipAddress;
    }
}
