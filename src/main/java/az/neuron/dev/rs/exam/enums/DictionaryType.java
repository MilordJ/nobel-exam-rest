/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.enums;

/**
 *
 * @author Nazrin
 */
public enum DictionaryType {
    GENDER,
    CITIZENSHIP,
    MARITAL_STATUS,
    SOCIAL_STATUS,
    ORPHAN_DEGREE,
    MILITARY_SERVICE,
    NATIONALITY,
    EDUCATION_DEGREE
}
