/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author Nazrin
 */

@Data
public class User {
    private int id;
    private UserAccount account;
    private String name;
    String middlename;
    private String surname;
    private String patronymic;
    private String birthdate;
    private DictionaryWrapper gender;
    private String pin;
    private FileWrapper image;
    private boolean sessionActive;

    public User() {
    }



    public User(int id, UserAccount account, String name, String surname, String patronymic, String birthdate, DictionaryWrapper gender, String pin, FileWrapper image, boolean sessionActive) {
        this.id = id;
        this.account = account;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.birthdate = birthdate;
        this.gender = gender;
        this.pin = pin;
        this.image = image;
        this.sessionActive = sessionActive;
    }

    public User(String name, String middlename, String surname) {
        this.name = name;
        this.middlename = middlename;
        this.surname = surname;
    }
    
    
}
