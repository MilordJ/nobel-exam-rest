/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.dao;

import az.neuron.dev.rs.exam.db.DbConnect;
import az.neuron.dev.rs.exam.domain.DictionaryWrapper;
import az.neuron.dev.rs.exam.domain.MultilanguageString;
import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.QuestionChoises;
import az.neuron.dev.rs.exam.domain.Questions;
import az.neuron.dev.rs.exam.domain.Ticket;
import az.neuron.dev.rs.exam.enums.ResultCode;
import az.neuron.dev.rs.exam.form.TicketForm;
import az.neuron.dev.rs.exam.util.Converter;
import az.neuron.dev.rs.exam.util.ErrorMessage;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author RUHANI ALIYEV
 */
@Repository
public class TicketDao implements ITicketDao {

    private static final Logger log = Logger.getLogger(TicketDao.class);

    @Autowired
    private DbConnect dbConnect;
    
    @Autowired
    private QuestionsDao questionsDao;

    @Override
    public OperationResponse addTicket(TicketForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try (OracleConnection connection = (OracleConnection) dbConnect.getConnection()) {
            try (CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.add_ticket_structures(?,?,?,?,?,?)}")) {
                cl.setString(1, form.getToken());
                cl.setInt(2, form.getTicketCount() == null ? 0 : form.getTicketCount());
                cl.setString(3, form.getQuestNote());
                cl.setArray(4, connection.createARRAY("UB_HSIS.TP_NUMBER_TABLE", form.getSubject()));
//                cl.setInt(5, form.getExamType()== null ? 0 : form.getExamType());
//                cl.setInt(6, form.getEduPlanId() == null ? 0 : form.getEduPlanId());
//                cl.setInt(7, form.getTicketDepartment() == null ? 0 : form.getTicketDepartment());
//                cl.setInt(8, form.getTicketFaculty() == null ? 0 : form.getTicketFaculty());
//                cl.setInt(9, form.getTicketGroup() == null ? 0 : form.getTicketGroup());
                cl.setArray(5, connection.createARRAY("UB_HSIS.TP_NUMBER_TABLE", form.getQuestTopic()));
                cl.setArray(6, connection.createARRAY("UB_EXAM.TP_QUEST_STR_NUMBER_TABLE", Converter.toOracleQuestStructureObject(form.getStructure())));

                cl.executeUpdate();

                operationResponse.setCode(ResultCode.OK);

            }
            
        } catch (SQLException e) {
            int code = e.getErrorCode();
            MultilanguageString message = ErrorMessage.message.get(code);
            if (message != null) {
                operationResponse.setMessage(message);
                log.info(message);
            }
            log.error(e.getMessage(), e);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public List<Ticket> getTickets(TicketForm form) {
        List<Ticket> list = new ArrayList<>();
        try(Connection connection =  dbConnect.getConnection()) {
            try(CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_ticket_structures(?,?,?,?,?,?,?,?,?)}")){
                
                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, form.getToken());
                cl.setInt(3, form.getSubjectId() == null ? 0 : form.getSubjectId());
                cl.setInt(4, form.getEduPlanId() == null ? 0 : form.getEduPlanId());
                cl.setInt(5, form.getTicketDepartment() == null ? 0 : form.getTicketDepartment());
                cl.setInt(6, form.getTicketFaculty() == null ? 0 : form.getTicketFaculty());
                cl.setInt(7, form.getTicketGroup() == null ? 0 : form.getTicketGroup());
                cl.setInt(8, form.getKeyword() == null ? 0 : form.getKeyword());
                cl.setInt(9, form.getPageSize());
                cl.setInt(10, form.getPage());   
                
                cl.execute();
                
                try(ResultSet rs = (ResultSet)cl.getObject(1)){
                    while(rs.next()){
                        list.add(new Ticket(rs.getInt("ID"), 
                                            rs.getString("NOTE")
//                                    new DictionaryWrapper(rs.getInt("SUBJECT_ID"), 
//                                        new MultilanguageString(rs.getString("SUBJECT_NAME_AZ"),
//                                                                rs.getString("SUBJECT_NAME_EN"),
//                                                                rs.getString("SUBJECT_NAME_RU")))
                        ));
                    }
                }
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public OperationResponse getTicketQuestions(String token, int ticketId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        Set<Questions> set =  new HashSet<>();
        try(Connection connection = dbConnect.getConnection()) {
            try(CallableStatement cl = connection.prepareCall("{? = call ub_exam.pkg_exam.get_ticket_structures_details(?,?)}")){
                
                cl.registerOutParameter(1, OracleTypes.CURSOR);
                cl.setString(2, token);
                cl.setInt(3, ticketId);
                
                cl.execute();
                
                try(ResultSet rs = (ResultSet)cl.getObject(1)){
                    while (rs.next()) {   
                        String path = null;
                        if (rs.getString("file_path") != null && !rs.getString("file_path").trim().isEmpty()) {
                            String[] imagePath = rs.getString("file_path").split("/");
                            path = imagePath[imagePath.length - 1].split("\\.")[0];
                        }
                        
                        set.add(new Questions( rs.getInt("ID"), 
                                                rs.getString("QUEST_CONTENT"), 
                                                path, 
//                                                new DictionaryWrapper(rs.getInt("SUBJECT_ID"), 
//                                                    new MultilanguageString(rs.getString("SUBJECT_NAME_AZ"), null, null)), 
//                                                new DictionaryWrapper(rs.getInt("UNI_ID"), 
//                                                    new MultilanguageString(rs.getString("UNI_NAME_AZ"), null, null)), 
                                                (List<QuestionChoises>) questionsDao.getQuestionChoises(token, rs.getInt("ID")).getData()));
                    }
                    operationResponse.setData(set);
                    operationResponse.setCode(ResultCode.OK);
                }
            }
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

    @Override
    public OperationResponse removeTicket(String token, int ticketId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try(Connection connection =  dbConnect.getConnection()) {
            try(CallableStatement cl = connection.prepareCall("{call ub_exam.pkg_exam.remove_ticket_structures(?,?)}")){
                
                cl.setString(1, token);
                cl.setInt(2, ticketId);
                cl.executeUpdate();
                
                operationResponse.setCode(ResultCode.OK);
            }
        } catch (SQLException exception) {
            int code = exception.getErrorCode();
            MultilanguageString ms = ErrorMessage.message.get(code);
            if (ms != null) {
                operationResponse.setMessage(ms);
                log.info(ms);
            }
            log.error(exception.getMessage(), exception);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }

}
