/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.dao;

import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.Questions;
import az.neuron.dev.rs.exam.domain.Ticket;
import az.neuron.dev.rs.exam.form.TicketForm;
import java.util.List;

/**
 *
 * @author RUHANI ALIYEV
 */
public interface ITicketDao {
    OperationResponse addTicket(TicketForm form);
    OperationResponse removeTicket(String token, int ticketId);
    List<Ticket> getTickets(TicketForm form);
    OperationResponse getTicketQuestions(String token, int ticketId);
}
