/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.form;

import lombok.Data;

/**
 *
 * @author Lito
 */

@Data
public class QuestStructureForm {
    private Integer questCount;
    private Integer questDifficulity;
    private Integer questSpec;
    private Integer questType;
    private Integer language;

    
}
