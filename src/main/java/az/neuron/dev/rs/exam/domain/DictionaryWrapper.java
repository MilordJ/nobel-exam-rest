/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.domain;

import lombok.Data;

/**
 *
 * @author Nazrin
 */
@Data
public class DictionaryWrapper {
    private int id;
    private MultilanguageString value;
    private int typeId;
    private int parentId;

    public DictionaryWrapper() {
    }

    public DictionaryWrapper(int id) {
        this.id = id;
    }
    
    public DictionaryWrapper(int id, MultilanguageString value, int typeId, int parentId) {
        this.id = id;
        this.value = value;
        this.typeId = typeId;
        this.parentId = parentId;
    }

    public DictionaryWrapper(int id, MultilanguageString value, int typeId) {
        this.id = id;
        this.value = value;
        this.typeId = typeId;
    }
    
   
    
    public DictionaryWrapper(int id, MultilanguageString value) {
        this.id = id;
        this.value = value;
    }


}
