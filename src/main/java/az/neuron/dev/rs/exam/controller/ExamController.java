/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.controller;

import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.enums.ResultCode;
import az.neuron.dev.rs.exam.form.BaseForm;
import az.neuron.dev.rs.exam.form.ExamForm;
import az.neuron.dev.rs.exam.form.ExamProcessForm;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ÜNAL CƏFƏRLİ
 */
@RestController
@RequestMapping(value = "/exam", produces = MediaType.APPLICATION_JSON_VALUE)
public class ExamController extends SkeletonController {
    private static final Logger log = Logger.getLogger(ExamController.class);
    
    @GetMapping(value = "/course/{courseId:\\d+}/fexams")
    protected OperationResponse getPartFinishedExamList(BaseForm form, HttpServletRequest request, @PathVariable int courseId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/course/{courseId:\\d+}/fexams [GET]. Form: " + form + ", user: " + user);
            operationResponse.setData(service.getPartFinishedExamList(form.getToken(), courseId));
            operationResponse.setCode(ResultCode.OK);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/user/finished/exlist")
    protected OperationResponse getParticipantFinishedExamsList(BaseForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/user/finished/exlist [GET]. Form: " + form + ", user: " + user);
            operationResponse = service.getParticipantFinishedExamsList(form.getToken());
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/finished/list")
    protected OperationResponse getAllFinishedExamsList(ExamForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/finished/list [GET]. Form: " + form + ", user: " + user);
            operationResponse = service.getAllFinishedExamsList(form);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/{examId:\\d+}/pelc/{pelcId:\\d+}/examing")
    protected OperationResponse getExaming(ExamForm form, 
                                            @PathVariable int pelcId, 
                                            @PathVariable int examId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/{examId:\\d+}/pelc/{pelcId:\\d+}/examing [GET]. Form: " + form + ", user: " + user);
            form.setId(examId);
            form.setPelcId(pelcId);
            operationResponse.setData(service.getExaming(form));
            operationResponse.setCode(ResultCode.OK);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/{examId:\\d+}")
    protected OperationResponse getExamListDetails(BaseForm form,
                                                   @PathVariable int examId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{examId:\\d+} [GET].Form: " + form + ", user: " + user);
            operationResponse.setData(service.getExamListDetails(form.getToken(), examId));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @GetMapping(value = "/epartId/{examParticipantId:\\d+}/questions/result")
    protected OperationResponse getFinishedExamQuestions(BaseForm form,
                                                   @PathVariable int examParticipantId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/epartId/{examParticipantId:\\d+}/questions/result [GET].Form: " + form + ", user: " + user);
            operationResponse.setData(service.getExamResult(form.getToken(), examParticipantId));
            operationResponse.setCode(ResultCode.OK);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @PostMapping(value = "/{examId:\\d+}/pelc/{pelcId:\\d+}/examCode/confirm")
    protected OperationResponse confirmExamCode(ExamForm form, 
                                                @PathVariable int pelcId, 
                                                @PathVariable int examId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/{examId:\\d+}/pelc/{pelcId:\\d+}/examCode/confirm [POST]. Form: " + form + ", user: " + user);
            form.setId(examId);
            form.setPelcId(pelcId);
            operationResponse = service.confirmExamCode(form);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/list")
    protected OperationResponse getExamList(ExamForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/list [GET]. Form: " + form + ", user: " + user);
            operationResponse.setData(service.getExamList(form));
            operationResponse.setCode(ResultCode.OK);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/subjects")
    protected OperationResponse getSubjects(BaseForm form) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/subjects [GET]. Form: " + form + ", user: " + user);
            operationResponse.setData(service.getSubjects(form));
            operationResponse.setCode(ResultCode.OK);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "")
    protected OperationResponse getExam(ExamForm form){
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(form.getToken());
            log.info("[GET]. Form: " + form + ", user: " + user);
            operationResponse.setData(service.getExam(form));
            operationResponse.setCode(ResultCode.OK);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/examList")
    protected OperationResponse getParticipantExamList(BaseForm form){
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(form.getToken());
            log.info("/examList [GET]. Form: " + form + ", user: " + user);
            operationResponse = service.getParticipantExamList(form.getToken());
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/add")
    protected OperationResponse addExam (@RequestPart(name = "examParams", required = false) ExamForm form){
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/add [POST]. Form: " + form + ", user: " + user);
            
            operationResponse = service.addExam(form);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/{examId:\\d+}/remind")
    protected OperationResponse remindeExam(BaseForm form, @PathVariable int examId){
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{examId:\\d+}/remind [POST].Form: " + form + ", user: " + user);
            operationResponse = service.reminderMessage(examId);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;
    }
    
    @PostMapping(value = "/{id:\\d+}/remove")
    protected OperationResponse removeExam(BaseForm form, @PathVariable int id) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);

        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{id:\\d+}/remove [POST].Form: " + form + ", user: " + user);
            operationResponse = service.removeExam(form.getToken(), id);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return operationResponse;

    }
    
    @PostMapping(value = "/{examId:\\d+}/update")
    protected OperationResponse updateExam (ExamForm form, 
                                            @PathVariable int examId){
        
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try {
            User user = checkToken(operationResponse, form.getToken());
            log.info("/{examId:\\d+}/update [POST]. Form: " + form + ", user: " + user);
            form.setId(examId);
            operationResponse = service.editExam(form);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/{examId:\\d+}/question/{questionId:\\d+}/confirm")
    protected OperationResponse editExamResult(ExamProcessForm form, 
                                                @PathVariable int examId, 
                                                @PathVariable int questionId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/{examId:\\d+}/question/{question:\\d+}/confirm [GET]. Form: " + form + ", user: " + user);
            form.setExamId(examId);
            form.setQuestionId(questionId);
            operationResponse = service.editExamResult(form);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/{examId:\\d+}/examParticipant/{examParticipantId:\\d+}/duration/edit")
    protected OperationResponse editExamDuration(ExamProcessForm form, 
                                                @PathVariable int examParticipantId,
                                                @PathVariable int examId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/{examId:\\d+}/examParticipant/{examParticipantId:\\d+}/duration/edit [POST]. Form: " + form + ", user: " + user);
            form.setExamId(examId);
            form.setExamParticipantId(examParticipantId);
            operationResponse = service.editExamDuration(form);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @PostMapping(value = "/{examId:\\d+}/examParticipant/{examParticipantId:\\d+}/time")
    protected OperationResponse editExamParticipantTimeDetails(ExamProcessForm form, 
                                                @PathVariable int examParticipantId,
                                                @PathVariable int examId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/{examId:\\d+}/examParticipant/{examParticipantId:\\d+}/time [POST]. Form: " + form + ", user: " + user);
            form.setExamId(examId);
            form.setExamParticipantId(examParticipantId);
            operationResponse = service.examParticipantTimeDetails(form);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
    @GetMapping(value = "/{examId:\\d+}/participant/{participantId:\\d+}/result")
    protected OperationResponse getExamFinishedResult(ExamProcessForm form, 
                                                    @PathVariable int examId,
                                                    @PathVariable int participantId) {
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        try{
            User user = checkToken(form.getToken());
            log.info("/{examId:\\d+}/participant/{participantId:\\d+}/result [GET]. Form: " + form + ", user: " + user);
            form.setExamId(examId);
            form.setParticipantId(participantId);
            
            operationResponse = service.getExamFinishedResult(form);
            
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }
        return operationResponse;
    }
    
}
