package az.neuron.dev.rs.exam.util;

import az.neuron.dev.rs.exam.form.QuestStructureForm;
import az.neuron.dev.rs.exam.form.VariantForm;

public class Converter {
        public static final Object[][] toOracleVariantObject(VariantForm[] variantForms) {
        Object[][] objects = new Object[variantForms.length][];
        
        for(int i=0; i<variantForms.length; i++) {
            objects[i] = new Object[4];
            objects[i][0] = variantForms[i].getId();
            objects[i][1] = variantForms[i].getContent();
            objects[i][2] = variantForms[i].getFileForm()!= null? variantForms[i].getFileForm().getId() : 0;
            objects[i][3] = variantForms[i].getRightChoise();
        }
        
        return objects;
    }
        
        public static final Object[][] toOracleQuestStructureObject(QuestStructureForm[] form) {
        Object[][] objects = new Object[form.length][];
        
        for(int i=0; i<form.length;i++){
            objects[i] = new Object[4];
            objects[i][0] = form[i].getQuestDifficulity();
            objects[i][1] = 1011743;
//            objects[i][1] = form[i].getQuestSpec();
//            objects[i][2] = form[i].getQuestType();
            objects[i][2] = form[i].getQuestCount();
            objects[i][3] = form[i].getLanguage();
        }
        
        return objects;
    }    
}