/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.enums;

/**
 *
 * @author Nazrin
 */
public class Constants {
    public static final String VALIDATOR_ADD = "add";
    public static final String VALIDATOR_EDIT = "edit";
    public static final String VALIDATOR_REMOVE = "remove";
    

    
    public static class DocumentTypes {
        public static final String SCHOOL = "school";
        public static final String PERSONAL = "personal";
        public static final String ACADEMIC = "academic";
        public static final String WORK = "work";
    }
    
    
}
