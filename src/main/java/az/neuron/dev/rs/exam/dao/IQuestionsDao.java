/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.dao;

import az.neuron.dev.rs.exam.domain.DictionaryWrapper;
import az.neuron.dev.rs.exam.domain.FileWrapper;
import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.Questions;
import az.neuron.dev.rs.exam.form.FileWrapperForm;
import az.neuron.dev.rs.exam.form.QuestionChoisesForm;
import az.neuron.dev.rs.exam.form.QuestionSearchForm;
import az.neuron.dev.rs.exam.form.TicketForm;
import java.util.List;

/**
 *
 * @author RUHANI ALIYEV
 */
public interface IQuestionsDao {
    OperationResponse addQuestion(QuestionSearchForm questionform);
    int addFile(String token,FileWrapperForm fileForm);
    List<Questions> getQuestions(QuestionSearchForm form);
    List<DictionaryWrapper> getTopics(int subjectId);
    OperationResponse editQuestion(QuestionSearchForm questionform);
    OperationResponse addImageToQuestions(QuestionChoisesForm form);
    OperationResponse getQuestionDetails(String token, int id);
    OperationResponse getQuestionChoises(String token, int id);
    OperationResponse getQuestionChoisesForExaminer(String token, int questId, int participantId);
    OperationResponse updateQuestion(int id, QuestionSearchForm form);
    OperationResponse updateQuestionChoises(QuestionChoisesForm form);
    OperationResponse removeFile(String token, String path);
    OperationResponse removeQuestion(String token, int id);
    FileWrapper getImageByPath(String path);
    
}
