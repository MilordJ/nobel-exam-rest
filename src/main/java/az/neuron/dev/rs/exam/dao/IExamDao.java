/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.dao;

import az.neuron.dev.rs.exam.domain.Exam;
import az.neuron.dev.rs.exam.domain.ExamFinishedResult;
import az.neuron.dev.rs.exam.domain.ExamResult;
import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.PelcExam;
import az.neuron.dev.rs.exam.domain.QuestAndAnswer;
import az.neuron.dev.rs.exam.domain.Subject;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.form.BaseForm;
import az.neuron.dev.rs.exam.form.ExamForm;
import az.neuron.dev.rs.exam.form.ExamProcessForm;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author RUHANI ALIYEV
 */
public interface IExamDao {
    User checkToken(String token);
    List<Subject> getSubjects(BaseForm form);
    List<Exam> getExam(BaseForm form);
    List<Exam> getExamList(ExamForm form);
    List<ExamResult> getParticipantAnswers(int exPartId, int questId, Connection con);
    OperationResponse getParticipantExamList(String token);
    List<ExamFinishedResult> getPartFinishedExamList(String token, int courseId);
    List<ExamResult> getExamResult(String token, int participantId);
    PelcExam getParticipantExam(ExamForm form);
    OperationResponse addExam(ExamForm form);
    OperationResponse reminderMessage(int examId);
    OperationResponse editExam(ExamForm form);
    OperationResponse removeQuestion(String token, int id);
    OperationResponse removeExam(String token, int id);
    OperationResponse confirmExamCode(ExamForm form);
    OperationResponse editExamResult(ExamProcessForm form);
    OperationResponse editExamDuration(ExamProcessForm form);
    OperationResponse examParticipantTimeDetails(ExamProcessForm form);
    OperationResponse getExamFinishedResult(ExamProcessForm form);
    OperationResponse getAllFinishedExamsList(ExamForm form);
    OperationResponse getParticipantFinishedExamsList(String token);
    Exam getExamListDetails(String token, int id);
}
