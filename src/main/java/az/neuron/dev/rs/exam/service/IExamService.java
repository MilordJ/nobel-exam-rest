/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.neuron.dev.rs.exam.service;

import az.neuron.dev.rs.exam.domain.DictionaryWrapper;
import az.neuron.dev.rs.exam.domain.Exam;
import az.neuron.dev.rs.exam.domain.ExamFinishedResult;
import az.neuron.dev.rs.exam.domain.ExamResult;
import az.neuron.dev.rs.exam.domain.FileWrapper;
import az.neuron.dev.rs.exam.domain.OperationResponse;
import az.neuron.dev.rs.exam.domain.PelcExam;
import az.neuron.dev.rs.exam.domain.Questions;
import az.neuron.dev.rs.exam.domain.Subject;
import az.neuron.dev.rs.exam.domain.Ticket;
import az.neuron.dev.rs.exam.domain.User;
import az.neuron.dev.rs.exam.form.BaseForm;
import az.neuron.dev.rs.exam.form.ExamForm;
import az.neuron.dev.rs.exam.form.ExamProcessForm;
import az.neuron.dev.rs.exam.form.QuestionChoisesForm;
import az.neuron.dev.rs.exam.form.QuestionSearchForm;
import az.neuron.dev.rs.exam.form.TicketForm;
import java.util.List;

/**
 *
 * @author RUHANI ALIYEV
 * @author2 ÜNAL CƏFƏRLİ
 */
public interface IExamService {
    User checkToken(String token);
    PelcExam getExaming(ExamForm form);
    Exam getExamListDetails(String token, int id);
    OperationResponse getExamFinishedResult(ExamProcessForm form);
    List<Questions> getQuestions(QuestionSearchForm form);
    List<Subject> getSubjects(BaseForm form);
    List<DictionaryWrapper> getTopics(int subjectId);
    List<Ticket> getTickets(TicketForm form);
    OperationResponse getTicketQuestions(String token, int ticketId);
    List<Exam> getExam(BaseForm form);
    List<Exam> getExamList(ExamForm form);
    List<ExamResult> getExamResult(String token, int examParticipantId);
    List<ExamFinishedResult> getPartFinishedExamList(String token, int courseId);
    OperationResponse addExam(ExamForm form);
    OperationResponse editExam(ExamForm form);
    OperationResponse addImageToQuestions(QuestionChoisesForm form);
    OperationResponse addTicket(TicketForm form);
    OperationResponse getQuestionDetails(String token, int id);
    OperationResponse getQuestionChoises(String token, int id);
    OperationResponse removeTicket(String token, int ticketId);
    OperationResponse removeQuestion(String token, int id);
    OperationResponse removeExam(String token, int id);
    OperationResponse removeFile(String token, String path);
    OperationResponse editExamResult(ExamProcessForm form);
    OperationResponse editExamDuration(ExamProcessForm form);
    OperationResponse examParticipantTimeDetails(ExamProcessForm form);
    OperationResponse getAllFinishedExamsList(ExamForm form);
    OperationResponse getParticipantFinishedExamsList(String token);
    OperationResponse addQuestion(QuestionSearchForm form);
    OperationResponse getParticipantExamList(String token);
    FileWrapper getImageByPath(String token, String path);
    OperationResponse editQuestion(QuestionSearchForm questionform);
    OperationResponse confirmExamCode(ExamForm form);
    OperationResponse reminderMessage(int examId);
}
